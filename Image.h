#pragma once
#include <string>
#include <glm\glm.hpp>
#include <vector>
#include "Color.h"

namespace tl
{
	namespace graphics
	{
		class Image
		{
		public:
			Image() noexcept;
			Image(const std::string& imageFilePath) noexcept;
			~Image() noexcept;

			void setPixel(Uint32 x, Uint32 y, const Color& color) noexcept;
			
			const glm::uvec2& getSize() const noexcept;
			Color getPixel(Uint32 x, Uint32 y) const noexcept;
			const Uint8* getPixelsPtr() const noexcept;
			
			void create(const glm::uvec2& size, const Color& color) noexcept;
			void create(const glm::uvec2& size, const Uint8* pixels) noexcept;
			bool loadImageFromFile(const std::string& imageFilePath) noexcept;


		private:
			std::vector<Uint8> m_pixels{};
			glm::uvec2 m_size{};

		};
	}
}