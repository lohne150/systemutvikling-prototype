#include <glm\gtc\matrix_transform.hpp>
#include "Camera.h"
#include "LogManager.h"
#include "Engine.h"

namespace tl
{
	namespace components
	{
		const std::string Camera::type = "Camera";

		Camera::Camera(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		Camera::Camera(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			tinyxml2::XMLElement* typeElement = xmlElement->FirstChildElement("type");

			if (typeElement)
			{
				std::string cameraType = typeElement->GetText();

				if (cameraType == "ortographic")
				{
					float left = 0;
					float right = 0;
					float bottom = 0;
					float top = 0;
					float zNear = 0;
					float zFar = 0;
						
					xmlElement->FirstChildElement("left")->QueryFloatText(&left);
					xmlElement->FirstChildElement("right")->QueryFloatText(&right);
					xmlElement->FirstChildElement("bottom")->QueryFloatText(&bottom);
					xmlElement->FirstChildElement("top")->QueryFloatText(&top);
					xmlElement->FirstChildElement("zNear")->QueryFloatText(&zNear);
					xmlElement->FirstChildElement("zFar")->QueryFloatText(&zFar);

					m_perspective = glm::ortho(left, right, bottom, top, zNear, zFar);
				}
				else if (cameraType == "perspective")
				{
					float fovy = 0;
					float zNear = 0;
					float zFar = 0;
					glm::vec2 windowSize = s_engine->getWindow()->getSize();

					xmlElement->FirstChildElement("fovy")->QueryFloatText(&fovy);
					fovy = glm::radians(fovy);
					xmlElement->FirstChildElement("zNear")->QueryFloatText(&zNear);
					xmlElement->FirstChildElement("zFar")->QueryFloatText(&zFar);

					m_perspective = glm::perspective(fovy, windowSize.x / windowSize.y, zNear, zFar);
				}
				else
				{
					//Cry
				}
			}

			s_engine->getWindow()->setCameraId(m_ownerId);
		}

		Camera::~Camera() noexcept
		{
		}

		glm::mat4 Camera::getView() const noexcept
		{
			core::Transform& transform = s_engine->getGameObject(m_ownerId).getTransform();
			return glm::lookAt(transform.getPosition(), transform.getPosition() + transform.getForwardDirection(), glm::vec3(0, 1, 0));;
		}

		glm::mat4 Camera::getProjectionView() const noexcept
		{
			return m_perspective * getView();
		}

		glm::mat4 Camera::getProjection() const noexcept
		{
			return m_perspective;
		}

		const std::string& Camera::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> Camera::clone() const noexcept
		{
			return std::make_unique<Camera>(*this);
		}

		void Camera::start(graphics::Window& window) noexcept
		{

		}

		void Camera::update(graphics::Window& window, float deltaTime) noexcept
		{
		}


		void Camera::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{

		}
	}
}

