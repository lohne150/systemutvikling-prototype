#pragma once
#include "IComponent.h"
#include "GameObject.h"

namespace tl
{
	namespace core
	{
		class ComponentFactory
		{
		public:
			ComponentFactory() noexcept;
			~ComponentFactory() noexcept;

			static std::unique_ptr<IComponent> createComponent(const std::string& componentType, Uint32 parentId) noexcept;
			static std::unique_ptr<IComponent> createComponent(tinyxml2::XMLElement* xmlElement, Uint32 parentId) noexcept;
		};
	}
}