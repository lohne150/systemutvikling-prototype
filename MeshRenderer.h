#pragma once
#include "Component.h"
#include "Mesh.h"

namespace tl
{
	namespace components
	{
		class MeshRenderer : public core::Component
		{
		public:
			MeshRenderer(Uint32 ownerId) noexcept;
			MeshRenderer(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~MeshRenderer() noexcept;

			void setMesh(const graphics::Mesh& mesh) noexcept;

			void loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			graphics::Mesh m_mesh{};

		};
	}
}
