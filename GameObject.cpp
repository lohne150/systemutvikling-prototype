#include "GameObject.h"
#include "ComponentFactory.h"
#include "Engine.h"
#include "Window.h"
#include "Camera.h"
#include "LogManager.h"
#include "ParticleEmitter.h"
#include "MeshRenderer.h"
#include <algorithm>

namespace tl
{
	namespace core
	{
		////////////////////////////////////////////////////////////
		// Static member data
		////////////////////////////////////////////////////////////
		system::observer_ptr<Engine> GameObject::s_engine;
		Uint32 GameObject::s_gameObjectCounter = 0;

		////////////////////////////////////////////////////////////
		GameObject::GameObject() noexcept
			: m_id(s_gameObjectCounter++)
			, m_transform(m_id)
		{
		}

		GameObject::GameObject(Uint32 parentId) noexcept
		{
		}

		////////////////////////////////////////////////////////////
		GameObject::GameObject(const GameObject& other) noexcept
			: m_id(s_gameObjectCounter)
			, m_transform(m_id)
		{
			LogManager::log("Creating copy of gameobject \"%s\"\n", other.getName().c_str());
			*this = other;
		}

		////////////////////////////////////////////////////////////
		GameObject::GameObject(tinyxml2::XMLElement* xmlElement) noexcept
			: m_id(s_gameObjectCounter++)
			, m_parentId(m_id)
			, m_transform(m_id, xmlElement->FirstChildElement("Transform"))
		{
			loadFromXML(xmlElement);
			m_transform.setOwnerId(m_id);
			m_transform.setParentId(m_parentId);
		}

		GameObject::GameObject(tinyxml2::XMLElement * xmlElement, Uint32 parentId) noexcept
			: m_id(s_gameObjectCounter++)
			, m_parentId(parentId)
			, m_transform(m_id, xmlElement->FirstChildElement("Transform"))
		{
			loadFromXML(xmlElement);
			m_transform.setOwnerId(m_id);
			m_transform.setParentId(m_parentId);
		}

		////////////////////////////////////////////////////////////
		GameObject::~GameObject() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		GameObject& GameObject::operator=(const GameObject & other) noexcept
		{
			m_parentId = other.m_parentId;
			m_id = other.m_id;
			m_transform = other.m_transform;
			m_name = other.m_name;

			for (auto&& component : other.m_components)
			{
				m_components.push_back(component->clone());
			}

			return *this;
		}

		////////////////////////////////////////////////////////////
		const std::string & GameObject::getName() const noexcept
		{
			return m_name;
		}

		////////////////////////////////////////////////////////////
		Transform & GameObject::getTransform() noexcept
		{
			return m_transform;
		}

		Uint32 GameObject::getParentId() const noexcept
		{
			return m_parentId;
		}

		////////////////////////////////////////////////////////////
		Uint32 GameObject::getId() const noexcept
		{
			return m_id;
		}

		////////////////////////////////////////////////////////////
		void GameObject::setName(const std::string & name) noexcept
		{
			LogManager::log("Changed name to \"%s\" from \"%s\"\n", name.c_str(), getName().c_str());

			m_name = name;
		}

		////////////////////////////////////////////////////////////
		void GameObject::setParentId(Uint32 parentId) noexcept
		{
			m_parentId = parentId;
			m_transform.setParentId(m_parentId);
		}

		////////////////////////////////////////////////////////////
		void GameObject::start(graphics::Window& window) noexcept
		{
			for (auto&& component : m_components)
			{
				component->start(window);
			}
		}

		////////////////////////////////////////////////////////////
		void GameObject::update(graphics::Window& window, float deltaTime) noexcept
		{
			m_transform.update();
			std::for_each(m_components.begin(), 
						  m_components.end(), 
						  [&window, deltaTime](std::unique_ptr<IComponent>& component) { component->update(window, deltaTime); });
		}

		////////////////////////////////////////////////////////////
		void GameObject::render(graphics::Window& window, graphics::Shader& shader) noexcept
		{
			std::for_each(m_components.begin(),
						  m_components.end(),
						  [&](auto& component) { component->render(window, shader, m_transform); });
		}

		////////////////////////////////////////////////////////////
		template<typename T>
		T& GameObject::getComponent() noexcept
		{
			components::MeshRenderer::type;
			return 'a';
		}

		////////////////////////////////////////////////////////////
		template <>
		components::Camera& GameObject::getComponent() noexcept
		{
			return *dynamic_cast<components::Camera*>(std::find_if(m_components.begin(), 
																   m_components.end(), 
																   [](auto& component) { return component->getComponentType() == components::Camera::type; })->get());
		}

		template <>
		components::ParticleEmitter& GameObject::getComponent() noexcept
		{
			for (auto&& component : m_components)
			{
				if (component->getComponentType() == components::ParticleEmitter::type)
				{
					return *dynamic_cast<components::ParticleEmitter*>(component.get());
				}
			}
		}

		Uint32 GameObject::getNextGameObjectId() noexcept
		{
			return s_gameObjectCounter;
		}

		////////////////////////////////////////////////////////////
		void GameObject::initializeEngine(system::observer_ptr<Engine> engine) noexcept
		{
			s_engine = engine;
		}

		GameObject GameObject::clone() noexcept
		{
			GameObject result = *this;
			result.generateNewId();
			return result;
		}

		void GameObject::generateNewId() noexcept
		{
			m_id = s_gameObjectCounter++;

			m_transform.setOwnerId(m_id);
			for (auto&& component : m_components)
			{
				component->setOwnerId(m_id);
			}
		}

		////////////////////////////////////////////////////////////
		void GameObject::loadFromXML(tinyxml2::XMLElement * xmlElement) noexcept
		{
			tinyxml2::XMLElement* components = xmlElement->FirstChildElement("Components");
			setName(xmlElement->Attribute("name"));

			if (components != nullptr)
			{
				tinyxml2::XMLElement* component = components->FirstChildElement();

				while (component != nullptr)
				{
					std::string name = component->Value();
					addComponent(ComponentFactory::createComponent(component, m_id));
					component = component->NextSiblingElement();
				}
			}

		}

		////////////////////////////////////////////////////////////
		void GameObject::addComponent(std::unique_ptr<IComponent> component) noexcept
		{
			m_components.push_back(std::move(component));
		}
	}
}