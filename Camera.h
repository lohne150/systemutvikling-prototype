#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class Camera : public core::Component
		{
		public:
			Camera(Uint32 ownerId) noexcept;
			Camera(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~Camera() noexcept;

			glm::mat4 getView() const noexcept;
			glm::mat4 getProjection() const noexcept;
			glm::mat4 getProjectionView() const noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			glm::mat4 m_perspective{ 1.0f };
			size_t m_id{};

			static size_t s_numberOfCameras;

		};
	}
}

