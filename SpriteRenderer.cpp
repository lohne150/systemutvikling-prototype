#include <glm\gtc\matrix_transform.hpp>
#include "SpriteRenderer.h"
#include "Transform.h"
#include "Camera.h"
#include "Mesh.h"

namespace tl
{
	namespace components
	{
		const std::string SpriteRenderer::type = "SpriteRenderer";

		SpriteRenderer::SpriteRenderer(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		SpriteRenderer::SpriteRenderer(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			if (xmlElement && xmlElement->Value() == type)
			{
				xmlElement->FirstChildElement("width")->QueryUnsignedText(&m_size.x);
				xmlElement->FirstChildElement("height")->QueryUnsignedText(&m_size.y);
	
				tinyxml2::XMLElement* textureElement = xmlElement->FirstChildElement("texture");
				if (textureElement)
				{
					std::string textureName = textureElement->GetText();
					m_texture = graphics::Texture(textureName);
				}
			}
		}


		SpriteRenderer::~SpriteRenderer() noexcept
		{
		}

		const std::string & SpriteRenderer::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> SpriteRenderer::clone() const noexcept
		{
			return std::make_unique<SpriteRenderer>(*this);
		}

		void SpriteRenderer::start(graphics::Window& window) noexcept
		{
		}

		void SpriteRenderer::update(graphics::Window& window, float deltaTime) noexcept
		{
		}

		void SpriteRenderer::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
			glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(m_size.x, 1.0f, m_size.y));

			shader.setUniform("modelMatrix", window.getCamera()->getProjectionView() * transform.getModelMatrix() * scale);
			m_texture.bind();
			m_mesh.draw();
		}
	}
}