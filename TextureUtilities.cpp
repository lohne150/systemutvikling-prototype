#include <vector>
#include <boost\filesystem.hpp>
#include "TextureUtilities.h"
#include "Config.h"
#include "XMLUtilities.h"

namespace tl
{
	////////////////////////////////////////////////////////////
	// Static member data
	////////////////////////////////////////////////////////////
	std::vector<std::string> s_supportedTextureTypes;

	////////////////////////////////////////////////////////////
	bool checkTexturesType(const std::string& imageType) noexcept
	{
		for (auto&& supportedType : s_supportedTextureTypes)
		{
			if (imageType == supportedType)
			{
				return true;
			}
		}

		return false;
	}

	////////////////////////////////////////////////////////////
	void loadSupportedTexturesTypes() noexcept
	{
		tinyxml2::XMLDocument document;
		document.LoadFile(SETTINGS_FILE_PATH);
		tinyxml2::XMLElement* typeElement = getElement(document, "Settings/Textures/SupportedTypes/Type");
		
		while (typeElement)
		{
			s_supportedTextureTypes.push_back(typeElement->GetText());
			typeElement = typeElement->NextSiblingElement("Type");
		}
	}
}