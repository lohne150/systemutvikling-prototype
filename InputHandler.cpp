#include <algorithm>
#include "InputHandler.h"
#include "LogManager.h"

namespace tl
{
	////////////////////////////////////////////////////////////
	// Static member data
	////////////////////////////////////////////////////////////
	glm::dvec2 InputHandler::s_mousePosition;
	glm::dvec2 InputHandler::s_oldMousePosition;
	std::vector<bool> InputHandler::s_currentKeys;
	std::vector<bool> InputHandler::s_upKeys;
	std::vector<bool> InputHandler::s_downKeys;
	std::vector<bool> InputHandler::s_currentButtons;
	std::vector<bool> InputHandler::s_upButtons;
	std::vector<bool> InputHandler::s_downButtons;
	std::shared_ptr<graphics::Window> InputHandler::s_window;

	////////////////////////////////////////////////////////////
	InputHandler::InputHandler() noexcept
	{
		m_activeKeys = 
		{
			Key::A,
			Key::B,
			Key::C,
			Key::D,
			Key::E,
			Key::F,
			Key::G,
			Key::H,
			Key::I,
			Key::J,
			Key::K,
			Key::L,
			Key::M,
			Key::N,
			Key::O,
			Key::P,
			Key::Q,
			Key::R,
			Key::S,
			Key::T,
			Key::U,
			Key::V,
			Key::W,
			Key::X,
			Key::Y,
			Key::Z,
			Key::Num0,
			Key::Num1,
			Key::Num2,
			Key::Num3,
			Key::Num4,
			Key::Num5,
			Key::Num6,
			Key::Num7,
			Key::Num8,
			Key::Num9,
			Key::Escape,
			Key::LControl,
			Key::LShift,
			Key::LAlt,
			Key::LSystem,
			Key::RControl,
			Key::RShift,
			Key::RAlt,
			Key::RSystem,
			Key::Menu,
			Key::LBracket,
			Key::RBracket,
			Key::SemiColon,
			Key::Comma,
			Key::Period,
			Key::Quote,
			Key::Slash,
			Key::BackSlash,
			Key::Equal,
			Key::Dash,
			Key::Space,
			Key::Return,
			Key::BackSpace,
			Key::Tab,
			Key::PageUp,
			Key::PageDown,
			Key::End,
			Key::Home,
			Key::Insert,
			Key::Delete,
			Key::Add,
			Key::Subtract,
			Key::Multiply,
			Key::Divide,
			Key::Left,
			Key::Right,
			Key::Up,
			Key::Down,
			Key::Numpad0,
			Key::Numpad1,
			Key::Numpad2,
			Key::Numpad3,
			Key::Numpad4,
			Key::Numpad5,
			Key::Numpad6,
			Key::Numpad7,
			Key::Numpad8,
			Key::Numpad9,
			Key::F1,
			Key::F2,
			Key::F3,
			Key::F4,
			Key::F5,
			Key::F6,
			Key::F7,
			Key::F8,
			Key::F9,
			Key::F10,
			Key::F11,
			Key::F12,
			Key::F13,
			Key::F14,
			Key::F15,
			Key::Pause,
		};

		m_activeButtons =
		{
			Button::Mouse1,
			Button::Mouse2,
			Button::Mouse3,
			Button::Mouse4,
			Button::Mouse5,
			Button::Mouse6,
			Button::Mouse7,
			Button::Mouse8
		};

		Uint32 keyCount = static_cast<Uint32>(Key::KeyCount);
		Uint32 buttonCount = static_cast<Uint32>(Button::ButtonCount);

		s_currentKeys = std::vector<bool>(keyCount, false);
		s_downKeys = std::vector<bool>(keyCount, false);
		s_upKeys = std::vector<bool>(keyCount, false);

		s_currentButtons = std::vector<bool>(buttonCount, false);
		s_downButtons = std::vector<bool>(buttonCount, false);
		s_upButtons = std::vector<bool>(buttonCount, false);
	}

	////////////////////////////////////////////////////////////
	InputHandler::~InputHandler() noexcept
	{
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isKeyPressed(Key keyCode) noexcept
	{
		return s_currentKeys[static_cast<Uint32>(keyCode)];
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isKeyUp(Key keyCode) noexcept
	{
		return s_upKeys[static_cast<Uint32>(keyCode)];
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isKeyDown(Key keyCode) noexcept
	{
		return s_downKeys[static_cast<Uint32>(keyCode)];
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isButtonPressed(Button buttonCode) noexcept
	{
		return s_currentButtons[static_cast<Uint32>(buttonCode)];
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isButtonUp(Button buttonCode) noexcept
	{
		return s_upButtons[static_cast<Uint32>(buttonCode)];
	}

	////////////////////////////////////////////////////////////
	bool InputHandler::isButtonDown(Button buttonCode) noexcept
	{
		return s_downButtons[static_cast<Uint32>(buttonCode)];
	}

	////////////////////////////////////////////////////////////
	glm::vec2 InputHandler::getMousePosition() noexcept
	{
		return s_mousePosition;
	}

	////////////////////////////////////////////////////////////
	glm::vec2 InputHandler::getMouseDelta() noexcept
	{
		return s_mousePosition - s_oldMousePosition;
	}

	////////////////////////////////////////////////////////////
	void InputHandler::initializeWindow(std::shared_ptr<graphics::Window> window) noexcept
	{
		LogManager::log("Initializing the inputhandlers window\n");
		s_window = window;
		glfwGetCursorPos(s_window->getWindowHandler(), &s_mousePosition.x, &s_mousePosition.y);
		s_oldMousePosition = s_mousePosition;
	}

	////////////////////////////////////////////////////////////
	void InputHandler::handleMouseInput() noexcept
	{
		s_oldMousePosition = s_mousePosition;
		glfwGetCursorPos(s_window->getWindowHandler(), &s_mousePosition.x, &s_mousePosition.y);

		for (auto& activeButton : m_activeButtons)
		{
			Int32 button = static_cast<Int32>(activeButton);
			Uint32 buttonStatus = glfwGetMouseButton(s_window->getWindowHandler(), button);

			if (buttonStatus == GLFW_PRESS && !s_currentButtons[button])
			{
				s_currentButtons[button] = true;
				s_downButtons[button] = true;
			}
			else
			{
				s_downButtons[button] = false;
			}

			if (buttonStatus == GLFW_RELEASE && s_currentButtons[button])
			{
				s_currentButtons[button] = false;
				s_upButtons[button] = true;
			}
			else
			{
				s_upButtons[button] = false;
			}
		}
	}

	////////////////////////////////////////////////////////////
	void InputHandler::handleEvents() noexcept
	{
		handleMouseInput();

		for (auto& activeKey : m_activeKeys)
		{
			Int32 key = static_cast<Int32>(activeKey);
			Uint32 keyStatus = glfwGetKey(s_window->getWindowHandler(), key);

			if (keyStatus == GLFW_PRESS && !s_currentKeys[key])
			{
				s_currentKeys[key] = true;
				s_downKeys[key] = true;
			}
			else
			{
				s_downKeys[key] = false;
			}

			if (keyStatus == GLFW_RELEASE && s_currentKeys[key])
			{
				s_currentKeys[key] = false;
				s_upKeys[key] = true;
			}
			else
			{
				s_upKeys[key] = false;
			}
		}
	}
}