#include <stdio.h>
#include <time.h>
#include <iomanip>
#include <algorithm>
#include <stdarg.h>
#include <chrono>
#include "LogManager.h"
#include "Config.h"
#include "StringUtils.h"
#include "XMLUtilities.h"

namespace tl
{
#pragma warning(push)
#pragma warning(disable: 4996)

	////////////////////////////////////////////////////////////
	// Static member data
	////////////////////////////////////////////////////////////
	std::string LogManager::s_path;

	const char * glTypeToString(GLenum type)
	{
		switch (type) {
		case GL_BOOL: return "bool";
		case GL_INT: return "int";
		case GL_FLOAT: return "float";
		case GL_FLOAT_VEC2: return "vec2";
		case GL_FLOAT_VEC3: return "vec3";
		case GL_FLOAT_VEC4: return "vec4";
		case GL_FLOAT_MAT2: return "mat2";
		case GL_FLOAT_MAT3: return "mat3";
		case GL_FLOAT_MAT4: return "mat4";
		case GL_SAMPLER_2D: return "sampler2D";
		case GL_SAMPLER_3D: return "sampler3D";
		case GL_SAMPLER_CUBE: return "samplerCube";
		case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
		default: break;
		}

		return "other";
	}


	void LogManager::initializeLogPath() noexcept
	{
		std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
		std::time_t now_c = std::chrono::system_clock::to_time_t(now);
		std::string time = std::ctime(&now_c);

		std::transform(time.begin(), time.end(), time.begin(),
			[](const char& ch)
		{
			if (ch == ' ')
			{
				return '_';
			}
			else if (ch == ':')
			{
				return '-';
			}
			else
			{
				return ch;
			}
		});
		removeCharFromString(time, '\n');

		tinyxml2::XMLDocument document;
		document.LoadFile(tl::SETTINGS_FILE_PATH);
		s_path = tl::getElementText(document, "Settings/Logging/Path") + "/log_" + time + ".txt";
	}

	bool LogManager::restartLog() noexcept
	{
#ifdef LOGGING
		LogManager::initializeLogPath();

		const char* cPath = s_path.c_str();
		FILE* file = fopen(cPath, "w");

		if (!file)
		{
			fprintf(stderr, "ERROR: could not open GL_LOG_FILE log file %s for writing\n\n", cPath);
			return false;
		}

		time_t now = time(NULL);
		char* date = ctime(&now);
		LogManager::log("GL_LOG_FILE log. local time %s\n", date);
		fclose(file);

		return true;
#endif
	}

	bool LogManager::log(const char * message, ...) noexcept
	{
#ifdef LOGGING
		const char* cPath = s_path.c_str();
		va_list argptr;
		FILE* file = fopen(cPath, "a");

		if (file)
		{
			//Prints the error message to the log file
			va_start(argptr, message);
			vfprintf(file, message, argptr);
			va_end(argptr);

#ifdef CONSOLEDEBUGGING
			//Prints the error message to the console window
			va_start(argptr, message);
			vfprintf(stderr, message, argptr);
			va_end(argptr);
#endif

			fclose(file);
		}
		else
		{
			fprintf(stderr, "ERROR: could not open GL_LOG_FILE %s file for appending\n", cPath);
			return false;
		}
#endif

		return true;
	}

	void LogManager::logError(const char * message, ...) noexcept
	{
#ifdef LOGGING
		const char* cPath = s_path.c_str();
		va_list argptr;
		FILE* file = fopen(cPath, "a");

		if (file)
		{
			//Prints the error message to the log file
			va_start(argptr, message);
			vfprintf(file, "-------------------------------------------------------------------------------\n", "");
			vfprintf(file, message, argptr);
			vfprintf(file, "-------------------------------------------------------------------------------\n", "");
			va_end(argptr);

#ifdef CONSOLEDEBUGGING
			//Prints the error message to the console window
			va_start(argptr, message);
			vfprintf(stderr, "-------------------------------------------------------------------------------\n", "");
			vfprintf(stderr, message, argptr);
			vfprintf(stderr, "-------------------------------------------------------------------------------\n", "");
			va_end(argptr);

			system("PAUSE");
#endif

			fclose(file);
		}
		else
		{
			fprintf(stderr, "ERROR: could not open GL_LOG_FILE %s file for appending\n", cPath);
		}
#endif

		exit(0);
	}

	void LogManager::logVersion() noexcept
	{
		const GLubyte* renderer = glGetString(GL_RENDERER);
		const GLubyte* version = glGetString(GL_VERSION);
		LogManager::log("\nVersion info:\n");
		LogManager::log("\tRenderer: %s\n", renderer);
		LogManager::log("\tOpenGL, version supported %s\n\n", version);
	}

	void LogManager::logAllInfo(GLuint program) noexcept
	{
		int params = -1;

		LogManager::log("\n-----------------------------\n\nshader program %i info:\n", program);

		//Prints the link status
		glGetProgramiv(program, GL_LINK_STATUS, &params);
		LogManager::log("GL_LINK_STATUS = %i\n", params);

		//Prints the attached shaders
		glGetProgramiv(program, GL_ATTACHED_SHADERS, &params);
		LogManager::log("GL_ATTACHED_SHADERS = %i\n", params);

		//Prints the active attributes
		glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &params);
		LogManager::log("GL_ACTIVE_ATTRIBUTES = %i\n", params);
		for (GLuint i = 0; i < static_cast<GLuint>(params); i++)
		{
			char name[64];
			int maxLen = 64;
			int actualLen = 0;
			int size = 0;
			GLenum type;

			glGetActiveAttrib(program, i, maxLen, &actualLen, &size, &type, name);
			for (int j = 0; j < size; j++)
			{
				int location = glGetAttribLocation(program, name);
				sprintf(name, "%s[%i]", name, j);
				LogManager::log("    %i) type:%s name:%s location:%i\n", i, glTypeToString(type), name, location);
			}
		}

		//Prints the active uniforms
		glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &params);
		LogManager::log("GL_ACTIVE_UNIFORMS = %i\n", params);
		for (GLuint i = 0; i < static_cast<GLuint>(params); i++)
		{
			char name[64];
			int maxLen = 64;
			int actualLen = 0;
			int size = 0;
			GLenum type;

			glGetActiveUniform(program, i, maxLen, &actualLen, &size, &type, name);
			for (int j = 0; j < size; j++)
			{
				int location = glGetUniformLocation(program, name);
				sprintf(name, "%s[%i]", name, j);
				LogManager::log("    %i) type:%s name:%s location:%i\n", i, glTypeToString(type), name, location);
			}
		}
	}

	void LogManager::logParams() noexcept
	{
		GLenum params[] = {
			GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
			GL_MAX_CUBE_MAP_TEXTURE_SIZE,
			GL_MAX_DRAW_BUFFERS,
			GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
			GL_MAX_TEXTURE_IMAGE_UNITS,
			GL_MAX_TEXTURE_SIZE,
			GL_MAX_VARYING_FLOATS,
			GL_MAX_VERTEX_ATTRIBS,
			GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
			GL_MAX_VERTEX_UNIFORM_COMPONENTS,
			GL_MAX_VIEWPORT_DIMS,
			GL_STEREO
		};

		const char* names[] = {
			"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
			"GL_MAX_CUBE_MAP_TEXTURE_SIZE",
			"GL_MAX_DRAW_BUFFERS",
			"GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
			"GL_MAX_TEXTURE_IMAGE_UNITS",
			"GL_MAX_TEXTURE_SIZE",
			"GL_MAX_VARYING_FLOATS",
			"GL_MAX_VERTEX_ATTRIBS",
			"GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
			"GL_MAX_VERTEX_UNIFORM_COMPONENTS",
			"GL_MAX_VIEWPORT_DIMS",
			"GL_STEREO"
		};

		LogManager::log("GL Context Params:\n");

		for (int i = 0; i < 10; i++)
		{
			int v = 0;
			glGetIntegerv(params[i], &v);
			LogManager::log("\t%s %i\n", names[i], v);
		}

		//Print max viewport dims
		int v[2] = { 0, 0 };
		glGetIntegerv(params[10], v);
		LogManager::log("\t%s %i %i\n", names[10], v[0], v[1]);

		//Print sterio
		unsigned char s = 0;
		glGetBooleanv(params[11], &s);
		LogManager::log("\t%s %u\n\n", names[11], (unsigned int)s);
	}
#pragma warning(pop)
}