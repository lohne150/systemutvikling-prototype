#pragma once
#include "Engine.h"
#include "IComponent.h"
#include "MapKey.h"
#include "observer_ptr.h"
#include "Transform.h"
#include "Shader.h"

namespace tl
{
	namespace core
	{
		class Component : public IComponent
		{
		public:
			Component(const std::string& componentType, Uint32 ownerId) noexcept;
			virtual ~Component() noexcept;

			virtual void setOwnerId(Uint32 ownerId) noexcept override;
			
			virtual Uint32 getOwnerId() const noexcept override;
			virtual Transform& getTransform() noexcept override;

			virtual const std::string& getComponentType() const noexcept = 0;
			virtual std::unique_ptr<IComponent> clone() const noexcept = 0;
			virtual void start(graphics::Window& window) noexcept = 0;
			virtual void update(graphics::Window& window, float deltaTime) noexcept = 0;
			virtual void render(graphics::Window& window, graphics::Shader& shader, Transform& transform) noexcept = 0;

			static void initializeEngine(system::observer_ptr<Engine> engine) noexcept;

		protected:
			Uint32 m_ownerId;

			static system::observer_ptr<Engine> s_engine;

		};
	}
}