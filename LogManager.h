#pragma once
#include <string>
#include "OpenGL.h"

namespace tl
{
	class LogManager
	{
	public:
		LogManager() = delete;
		~LogManager() = delete;

		static void initializeLogPath() noexcept;

		static bool restartLog() noexcept;
		static bool log(const char* message, ...) noexcept;
		static void logError(const char* message, ...) noexcept;
		static void logVersion() noexcept;
		static void logAllInfo(GLuint program) noexcept;
		static void logParams() noexcept;

	private:
		static std::string s_path;

	};
}