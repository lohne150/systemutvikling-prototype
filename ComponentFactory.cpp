#include "ComponentFactory.h"
#include "LogManager.h"
#include "SpriteRenderer.h"
#include "Camera.h"
#include "FreeLook.h"
#include "FreeMove.h"
#include "MouseLook.h"
#include "MeshRenderer.h"
#include "Shooter.h"
#include "ParticleEmitter.h"
#include "RigidBody.h"

namespace tl
{
	namespace core
	{
		ComponentFactory::ComponentFactory() noexcept
		{
		}


		ComponentFactory::~ComponentFactory() noexcept
		{
		}

		std::unique_ptr<IComponent> ComponentFactory::createComponent(const std::string& componentType, Uint32 parentId) noexcept
		{
			if (componentType == "SpriteRenderer")
			{
				return std::make_unique<components::SpriteRenderer>(parentId);
			}
			else
			{
				throw 0; //TODO
			}
		}
		std::unique_ptr<IComponent> ComponentFactory::createComponent(tinyxml2::XMLElement* xmlElement, Uint32 parentId) noexcept
		{
			std::string componentType = xmlElement->Value();

			if (componentType == "SpriteRenderer")
			{
				return std::make_unique<components::SpriteRenderer>(xmlElement, parentId);
			}
			else if (componentType == "Camera")
			{
				return std::make_unique<components::Camera>(xmlElement, parentId);
			}
			else if (componentType == "FreeLook")
			{
				return std::make_unique<components::FreeLook>(xmlElement, parentId);
			}
			else if (componentType == "FreeMove")
			{
				return std::make_unique<components::FreeMove>(xmlElement, parentId);
			}
			else if (componentType == "MouseLook")
			{
				return std::make_unique<components::MouseLook>(xmlElement, parentId);
			}
			else if (componentType == "MeshRenderer")
			{
				return std::make_unique<components::MeshRenderer>(xmlElement, parentId);
			}
			else if (componentType == "Shooter")
			{
				return std::make_unique<components::Shooter>(xmlElement, parentId);
			}
			else if (componentType == "ParticleEmitter")
			{
				return std::make_unique<components::ParticleEmitter>(xmlElement, parentId);
			}
			else if (componentType == "RigidBody")
			{
				return std::make_unique<components::RigidBody>(xmlElement, parentId);
			}
			else
			{
				throw 0; //TODO
			}
		}
	}
}