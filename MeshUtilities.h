#pragma once
#include <string>
#include "tinyxml2.h"

namespace tl
{
	bool checkMeshType(const std::string& meshType) noexcept;
	void loadSupportedMeshTypes() noexcept;
}