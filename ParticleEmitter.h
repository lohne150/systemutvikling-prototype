#pragma once
#include "Component.h"
#include "Mesh.h"
#include "Texture.h"
#include "ParticleMaster.h"
#include "Particle.h"

namespace tl
{
	namespace components
	{
		class ParticleEmitter : public core::Component
		{
		public:
			ParticleEmitter(Uint32 ownerId) noexcept;
			ParticleEmitter(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~ParticleEmitter() noexcept;

			void setTexture(const graphics::Texture& texture) noexcept;

			void loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept;
			glm::vec3 generateRandomUnitVector() noexcept;
			void emitParticle(graphics::Window& window) noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;
			
			static void initializeParticleMaster(std::shared_ptr<graphics::ParticleMaster> particleMaster) noexcept;
			static const std::string type;

		private:
			std::vector<graphics::Particle> m_particles;
			float m_duration{ 1.0f };
			float m_emissionRate{ 10000.0f };
			float m_maxForce{ 10.0f };
			float m_minForce{ 6.0f };
			float m_sinceLastEmission{};
			Uint32 m_maxParticles{ 3000 };

			graphics::Mesh m_mesh{ "particle" };
			graphics::Texture m_texture{ "star" };
			graphics::Shader m_shader{ "particle" };

			static std::shared_ptr<graphics::ParticleMaster> s_particleMaster;

		};
	}
}
