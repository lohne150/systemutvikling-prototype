#include "Shooter.h"
#include "InputHandler.h"
#include "MeshRenderer.h"
#include "RigidBody.h"
#include "ParticleEmitter.h"

namespace tl
{
	namespace components
	{
		const std::string Shooter::type = "Shooter";

		Shooter::Shooter(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		Shooter::Shooter(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		Shooter::~Shooter() noexcept
		{
		}

		const std::string& Shooter::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> Shooter::clone() const noexcept
		{
			return std::make_unique<Shooter>(*this);
		}

		void Shooter::start(graphics::Window& window) noexcept
		{
		}

		void Shooter::update(graphics::Window& window, float deltaTime) noexcept
		{
			if (InputHandler::isButtonDown(Button::RMouse))
			{
				core::GameObject object = s_engine->getGameObject("cube").clone();
				core::Transform& ownerTransform = s_engine->getGameObject(m_ownerId).getTransform();

				object.getTransform().setPosition(ownerTransform.getLocalPosition());
				object.getTransform().setScale(glm::vec3(2, 2, 2));
				object.getComponent<components::ParticleEmitter>().setTexture(graphics::Texture("NULL"));
				object.setParentId(s_engine->getGameObject("Stuff1").getId());


				auto rb = std::make_unique<components::RigidBody>(object.getId());
				rb->setVelocity(ownerTransform.getLocalForwardDirection() * 100.0f);
				object.addComponent(std::move(rb));

				s_engine->addGameObject(object);
			}
		}

		void Shooter::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}