#pragma once
#include <memory>
#include <string>
#include "Window.h"

namespace tl
{
	namespace graphics
	{
		class Shader;
	}

	namespace core
	{
		class Transform;

		class IComponent
		{
		public:
			IComponent() noexcept;
			virtual ~IComponent() noexcept;

			virtual void setOwnerId(Uint32 ownerId) noexcept = 0;

			virtual Uint32 getOwnerId() const noexcept = 0;
			virtual Transform& getTransform() noexcept = 0;

			virtual const std::string& getComponentType() const noexcept = 0;
			virtual std::unique_ptr<IComponent> clone() const noexcept = 0;
			virtual void start(graphics::Window& window) noexcept = 0;
			virtual void update(graphics::Window& window, float deltaTime) noexcept = 0;
			virtual void render(graphics::Window& window, graphics::Shader& shader, Transform& transform) noexcept = 0;
		};
	}
}