#include <sstream>
#include <algorithm>
#include "StringUtils.h"

namespace tl
{
	std::vector<std::string> splitString(const std::string& text, char delimiter) noexcept
	{
		std::vector<std::string> result;
		std::stringstream stringStream(text);
		std::string token;

		while (getline(stringStream, token, delimiter))
		{
			if (token.size() > 0)
			{
				result.push_back(token);
			}
		}

		return result;
	}

	void removeCharFromString(std::string& text, char character) noexcept
	{
		text.erase(std::remove(text.begin(), text.end(), character), text.end());
	}
}