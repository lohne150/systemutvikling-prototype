GL_LOG_FILE log. local time Thu Mar 31 15:40:09 2016

Initializing core engine
Initializing rendering engine
Initializing glfw...done!
Creating window...done!
Rendering engine done!
Initializing the inputhandlers window

Version info:
	Renderer: GeForce GTX 860M/PCIe/SSE2
	OpenGL, version supported 4.4.0

GL Context Params:
	GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 192
	GL_MAX_CUBE_MAP_TEXTURE_SIZE 16384
	GL_MAX_DRAW_BUFFERS 8
	GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 2048
	GL_MAX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_TEXTURE_SIZE 16384
	GL_MAX_VARYING_FLOATS 124
	GL_MAX_VERTEX_ATTRIBS 16
	GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_VERTEX_UNIFORM_COMPONENTS 4096
	GL_MAX_VIEWPORT_DIMS 16384 16384
	GL_STEREO 0

Initializing the components core engine
Loading all textures from res/textures
Creating texture "fire"
Loading image from path "res/textures\fire.png"...loaded!
Creating texture "mh"
Loading image from path "res/textures\mh.jpg"...loaded!
Creating texture "NULL"
Loading image from path "res/textures\NULL.png"...loaded!
Creating texture "smoke"
Loading image from path "res/textures\smoke.png"...loaded!
Creating texture "test"
Loading image from path "res/textures\test.png"...loaded!
Creating texture "test2"
Loading image from path "res/textures\test2.png"...loaded!
Textures loading done!
Loading all meshes from res/meshes
Core engine initialization done!
Creating image of size (100, 200) and color (128, 128, 255, 255)
Changed name to "Stuff1" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff1"
Constructing component of type "RigidBody" in gameobject "Stuff1"
Creating copy of gameobject "Stuff1"
Changed name to "Stuff3" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff3"
Creating copy of gameobject "Stuff3"
Changed name to "Stuff2" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff2"
Creating copy of gameobject "Stuff2"
Changed name to "look" from ""
Constructing component of type "MouseLook" in gameobject "look"
Constructing component of type "Shooter" in gameobject "look"
Constructing component of type "FreeMove" in gameobject "look"
Constructing component of type "Camera" in gameobject "look"
Creating copy of gameobject "look"
Changed name to "cube" from ""
Constructing component of type "MeshRenderer" in gameobject "cube"
Creating copy of gameobject "cube"
FPS: 32
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube1" from "cube"
Setting parentkey in component "MeshRenderer" to "cube1"
Setting parentkey in component "RigidBody" to "cube1"
Creating copy of gameobject "cube1"
FPS: 60
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube2" from "cube"
Setting parentkey in component "MeshRenderer" to "cube2"
Setting parentkey in component "RigidBody" to "cube2"
Creating copy of gameobject "cube2"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube3" from "cube"
Setting parentkey in component "MeshRenderer" to "cube3"
Setting parentkey in component "RigidBody" to "cube3"
Creating copy of gameobject "cube3"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube4" from "cube"
Setting parentkey in component "MeshRenderer" to "cube4"
Setting parentkey in component "RigidBody" to "cube4"
Creating copy of gameobject "cube4"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube5" from "cube"
Setting parentkey in component "MeshRenderer" to "cube5"
Setting parentkey in component "RigidBody" to "cube5"
Creating copy of gameobject "cube5"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube6" from "cube"
Setting parentkey in component "MeshRenderer" to "cube6"
Setting parentkey in component "RigidBody" to "cube6"
Creating copy of gameobject "cube6"
FPS: 60
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube7" from "cube"
Setting parentkey in component "MeshRenderer" to "cube7"
Setting parentkey in component "RigidBody" to "cube7"
Creating copy of gameobject "cube7"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube8" from "cube"
Setting parentkey in component "MeshRenderer" to "cube8"
Setting parentkey in component "RigidBody" to "cube8"
Creating copy of gameobject "cube8"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube9" from "cube"
Setting parentkey in component "MeshRenderer" to "cube9"
Setting parentkey in component "RigidBody" to "cube9"
Creating copy of gameobject "cube9"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube10" from "cube"
Setting parentkey in component "MeshRenderer" to "cube10"
Setting parentkey in component "RigidBody" to "cube10"
Creating copy of gameobject "cube10"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube11" from "cube"
Setting parentkey in component "MeshRenderer" to "cube11"
Setting parentkey in component "RigidBody" to "cube11"
Creating copy of gameobject "cube11"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube12" from "cube"
Setting parentkey in component "MeshRenderer" to "cube12"
Setting parentkey in component "RigidBody" to "cube12"
Creating copy of gameobject "cube12"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube13" from "cube"
Setting parentkey in component "MeshRenderer" to "cube13"
Setting parentkey in component "RigidBody" to "cube13"
Creating copy of gameobject "cube13"
FPS: 60
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube14" from "cube"
Setting parentkey in component "MeshRenderer" to "cube14"
Setting parentkey in component "RigidBody" to "cube14"
Creating copy of gameobject "cube14"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube15" from "cube"
Setting parentkey in component "MeshRenderer" to "cube15"
Setting parentkey in component "RigidBody" to "cube15"
Creating copy of gameobject "cube15"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube16" from "cube"
Setting parentkey in component "MeshRenderer" to "cube16"
Setting parentkey in component "RigidBody" to "cube16"
Creating copy of gameobject "cube16"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube17" from "cube"
Setting parentkey in component "MeshRenderer" to "cube17"
Setting parentkey in component "RigidBody" to "cube17"
Creating copy of gameobject "cube17"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube18" from "cube"
Setting parentkey in component "MeshRenderer" to "cube18"
Setting parentkey in component "RigidBody" to "cube18"
Creating copy of gameobject "cube18"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube19" from "cube"
Setting parentkey in component "MeshRenderer" to "cube19"
Setting parentkey in component "RigidBody" to "cube19"
Creating copy of gameobject "cube19"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube20" from "cube"
Setting parentkey in component "MeshRenderer" to "cube20"
Setting parentkey in component "RigidBody" to "cube20"
Creating copy of gameobject "cube20"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube21" from "cube"
Setting parentkey in component "MeshRenderer" to "cube21"
Setting parentkey in component "RigidBody" to "cube21"
Creating copy of gameobject "cube21"
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
