GL_LOG_FILE log. local time Wed Apr 27 14:59:42 2016

Initializing core engine
Initializing rendering engine
Initializing glfw...done!
Creating window...done!
Rendering engine done!
Initializing the inputhandlers window

Version info:
	Renderer: GeForce GTX 860M/PCIe/SSE2
	OpenGL, version supported 4.4.0

GL Context Params:
	GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 192
	GL_MAX_CUBE_MAP_TEXTURE_SIZE 16384
	GL_MAX_DRAW_BUFFERS 8
	GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 2048
	GL_MAX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_TEXTURE_SIZE 16384
	GL_MAX_VARYING_FLOATS 124
	GL_MAX_VERTEX_ATTRIBS 16
	GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_VERTEX_UNIFORM_COMPONENTS 4096
	GL_MAX_VIEWPORT_DIMS 16384 16384
	GL_STEREO 0

Initializing the components core engine
Loading all textures from res/textures
Creating texture "fire"
Loading image from path "res/textures\fire.png"...loaded!
Creating texture "mh"
Loading image from path "res/textures\mh.jpg"...loaded!
Creating texture "NULL"
Loading image from path "res/textures\NULL.png"...loaded!
Creating texture "smoke"
Loading image from path "res/textures\smoke.png"...loaded!
Creating texture "star"
Loading image from path "res/textures\star.png"...loaded!
Creating texture "test"
Loading image from path "res/textures\test.png"...loaded!
Creating texture "test2"
Loading image from path "res/textures\test2.png"...loaded!
Textures loading done!
Loading all meshes from res/meshes
Core engine initialization done!
Creating image of size (100, 200) and color (128, 128, 255, 255)
Changed name to "look" from ""
Constructing component of type "MouseLook" in gameobject with id "0"
Constructing component of type "FreeMove" in gameobject with id "0"
Constructing component of type "Camera" in gameobject with id "0"
Creating copy of gameobject "look"
Changed name to "cube" from ""
Constructing component of type "ParticleEmitter" in gameobject with id "1"
Creating copy of gameobject "cube"
FPS: 1
FPS: 1
FPS: 1
FPS: 1
FPS: 1
FPS: 1
FPS: 1
FPS: 21
FPS: 11
FPS: 8
FPS: 8
FPS: 7
FPS: 8
FPS: 7
FPS: 8
FPS: 8
FPS: 7
