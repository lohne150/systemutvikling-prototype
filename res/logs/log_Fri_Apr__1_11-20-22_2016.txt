GL_LOG_FILE log. local time Fri Apr  1 11:20:22 2016

Initializing core engine
Initializing rendering engine
Initializing glfw...done!
Creating window...done!
Rendering engine done!
Initializing the inputhandlers window

Version info:
	Renderer: GeForce GTX 860M/PCIe/SSE2
	OpenGL, version supported 4.4.0

GL Context Params:
	GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 192
	GL_MAX_CUBE_MAP_TEXTURE_SIZE 16384
	GL_MAX_DRAW_BUFFERS 8
	GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 2048
	GL_MAX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_TEXTURE_SIZE 16384
	GL_MAX_VARYING_FLOATS 124
	GL_MAX_VERTEX_ATTRIBS 16
	GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_VERTEX_UNIFORM_COMPONENTS 4096
	GL_MAX_VIEWPORT_DIMS 16384 16384
	GL_STEREO 0

Initializing the components core engine
Loading all textures from res/textures
Creating texture "fire"
Loading image from path "res/textures\fire.png"...loaded!
Creating texture "mh"
Loading image from path "res/textures\mh.jpg"...loaded!
Creating texture "NULL"
Loading image from path "res/textures\NULL.png"...loaded!
Creating texture "smoke"
Loading image from path "res/textures\smoke.png"...loaded!
Creating texture "test"
Loading image from path "res/textures\test.png"...loaded!
Creating texture "test2"
Loading image from path "res/textures\test2.png"...loaded!
Textures loading done!
Loading all meshes from res/meshes
Core engine initialization done!
Creating image of size (100, 200) and color (128, 128, 255, 255)
Changed name to "Stuff1" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff1"
Constructing component of type "RigidBody" in gameobject "Stuff1"
Creating copy of gameobject "Stuff1"
Changed name to "Stuff3" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff3"
Creating copy of gameobject "Stuff3"
Changed name to "Stuff2" from ""
Constructing component of type "SpriteRenderer" in gameobject "Stuff2"
Creating copy of gameobject "Stuff2"
Changed name to "look" from ""
Constructing component of type "MouseLook" in gameobject "look"
Constructing component of type "Shooter" in gameobject "look"
Constructing component of type "FreeMove" in gameobject "look"
Constructing component of type "Camera" in gameobject "look"
Creating copy of gameobject "look"
Changed name to "cube" from ""
Constructing component of type "MeshRenderer" in gameobject "cube"
Constructing component of type "ParticleEmitter" in gameobject "cube"
Creating copy of gameobject "cube"
FPS: 17
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 60
FPS: 61
FPS: 60
FPS: 60
FPS: 60
FPS: 56
FPS: 38
FPS: 39
FPS: 46
FPS: 57
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube1" from "cube"
Setting parentkey in component "MeshRenderer" to "cube1"
Setting parentkey in component "ParticleEmitter" to "cube1"
Setting parentkey in component "RigidBody" to "cube1"
Creating copy of gameobject "cube1"
FPS: 44
FPS: 36
FPS: 36
FPS: 38
FPS: 38
FPS: 39
FPS: 38
FPS: 36
FPS: 39
FPS: 38
FPS: 38
FPS: 33
FPS: 32
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube2" from "cube"
Setting parentkey in component "MeshRenderer" to "cube2"
Setting parentkey in component "ParticleEmitter" to "cube2"
Setting parentkey in component "RigidBody" to "cube2"
Creating copy of gameobject "cube2"
FPS: 33
FPS: 25
FPS: 25
FPS: 27
FPS: 25
FPS: 27
FPS: 23
FPS: 24
FPS: 25
FPS: 23
FPS: 25
FPS: 23
FPS: 22
FPS: 22
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube3" from "cube"
Setting parentkey in component "MeshRenderer" to "cube3"
Setting parentkey in component "ParticleEmitter" to "cube3"
Setting parentkey in component "RigidBody" to "cube3"
Creating copy of gameobject "cube3"
FPS: 22
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube4" from "cube"
Setting parentkey in component "MeshRenderer" to "cube4"
Setting parentkey in component "ParticleEmitter" to "cube4"
Setting parentkey in component "RigidBody" to "cube4"
Creating copy of gameobject "cube4"
FPS: 15
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube5" from "cube"
Setting parentkey in component "MeshRenderer" to "cube5"
Setting parentkey in component "ParticleEmitter" to "cube5"
Setting parentkey in component "RigidBody" to "cube5"
Creating copy of gameobject "cube5"
FPS: 16
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube6" from "cube"
Setting parentkey in component "MeshRenderer" to "cube6"
Setting parentkey in component "ParticleEmitter" to "cube6"
Setting parentkey in component "RigidBody" to "cube6"
Creating copy of gameobject "cube6"
FPS: 12
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube7" from "cube"
Setting parentkey in component "MeshRenderer" to "cube7"
Setting parentkey in component "ParticleEmitter" to "cube7"
Setting parentkey in component "RigidBody" to "cube7"
Creating copy of gameobject "cube7"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube8" from "cube"
Setting parentkey in component "MeshRenderer" to "cube8"
Setting parentkey in component "ParticleEmitter" to "cube8"
Setting parentkey in component "RigidBody" to "cube8"
Creating copy of gameobject "cube8"
FPS: 8
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube9" from "cube"
Setting parentkey in component "MeshRenderer" to "cube9"
Setting parentkey in component "ParticleEmitter" to "cube9"
Setting parentkey in component "RigidBody" to "cube9"
Creating copy of gameobject "cube9"
Creating copy of gameobject "cube"
Constructing component of type "RigidBody" in gameobject "cube"
Changed name to "cube10" from "cube"
Setting parentkey in component "MeshRenderer" to "cube10"
Setting parentkey in component "ParticleEmitter" to "cube10"
Setting parentkey in component "RigidBody" to "cube10"
Creating copy of gameobject "cube10"
FPS: 7
FPS: 6
FPS: 7
FPS: 6
FPS: 8
FPS: 9
FPS: 8
FPS: 9
FPS: 10
FPS: 9
