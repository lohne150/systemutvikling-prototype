GL_LOG_FILE log. local time Mon Mar  7 09:29:15 2016

Initializing core engine
Initializing rendering engine
Initializing glfw...done!
Creating window...done!
Rendering engine done!

Version info:
	Renderer: GeForce GTX 860M/PCIe/SSE2
	OpenGL, version supported 4.4.0

GL Context Params:
	GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 192
	GL_MAX_CUBE_MAP_TEXTURE_SIZE 16384
	GL_MAX_DRAW_BUFFERS 8
	GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 2048
	GL_MAX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_TEXTURE_SIZE 16384
	GL_MAX_VARYING_FLOATS 124
	GL_MAX_VERTEX_ATTRIBS 16
	GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 32
	GL_MAX_VERTEX_UNIFORM_COMPONENTS 4096
	GL_MAX_VIEWPORT_DIMS 16384 16384
	GL_STEREO 0

Initializing the components core engine
Loading all textures from res/images
Creating texture "mh"
Loading image from path "res/images\mh.jpg"...loaded!
Creating texture "NULL"
Loading image from path "res/images\NULL.png"...loaded!
Creating texture "test"
Loading image from path "res/images\test.png"...loaded!
Creating texture "test2"
Loading image from path "res/images\test2.png"...loaded!
Textures loading done!
Loading all meshes from res/meshes
Core engine initialization done!
Creating image of size (100, 200) and color (128, 128, 255, 255)
Constructing component of type "SpriteRenderer" in gameobject "Stuff1"
Constructing component of type "Camera" in gameobject "Stuff1"
Constructing component of type "SpriteRenderer" in gameobject "Stuff3"
Constructing component of type "SpriteRenderer" in gameobject "Stuff2"
