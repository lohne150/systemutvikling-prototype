<Scene>
	<GameObjects>
		<GameObject name="look">
			<Transform>
				<position_X>0</position_X>
				<position_Y>0</position_Y>
				<position_Z>0</position_Z>
			
				<rotation_X>0</rotation_X>
				<rotation_Y>0</rotation_Y>
				<rotation_Z>0</rotation_Z>
				
				<scale_X>1</scale_X>
				<scale_Y>1</scale_Y>
				<scale_Z>1</scale_Z>
			</Transform>
	
			<Components>
				<MouseLook/>
				<Shooter/>
				
				<FreeMove>
					<speed>10</speed>
				</FreeMove>

				<Camera>
					<type>perspective</type>
					<fovy>45</fovy>
					<zNear>0.1</zNear>
					<zFar>10000</zFar>
				</Camera>
			</Components>		
		</GameObject>
		
		<GameObject name="cube">
			<Transform>
				<position_X>20</position_X>
				<position_Y>20</position_Y>
				<position_Z>50</position_Z>
			
				<rotation_X>0</rotation_X>
			
				<scale_X>2</scale_X>
				<scale_Y>2</scale_Y>
				<scale_Z>2</scale_Z>
			</Transform>
			
			<Components>

				<ParticleEmitter>
					
				</ParticleEmitter>
			</Components>
		</GameObject>
	</GameObjects>
</Scene>