#version 330

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 position;
layout (location = 3) in float visibility;
layout (location = 4) in uint textureId;

flat out float visibility0;
out vec2 texCoord0;
out uint textureId0;

uniform mat4 modelMatrix;

void main()
{
	visibility0 = visibility;
	texCoord0 = texCoord;
	textureId0 = textureId;
	gl_Position = modelMatrix * vec4(vertex + position, 1);
}