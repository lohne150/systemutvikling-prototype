#version 330
#include "lighting.glh"

in vec2 texCoord0;
in vec4 normal0;
in vec4 worldPos0;

out vec4 fragColor;

uniform sampler2D R_diffuse;
uniform DirectionalLight R_directionalLight;
uniform float R_scalingFactor;
uniform float height;


void main()
{ 
	vec2 texCoord = texCoord0;

	if (texCoord.y > height * R_scalingFactor - 0.5 && normal0.y < 0.99)
	{
		texCoord.x += 0.33333333;
		texCoord.y -= int(height * R_scalingFactor * 1000000) % 1000000 / 1000000.0;
	}

    fragColor = calcDirectionalLight(R_directionalLight, normalize(normal0), worldPos0) * texture(R_diffuse, texCoord.xy);
}