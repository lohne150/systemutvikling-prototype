#version 330
#include "lighting.glh"

in vec2 texCoord0;
in vec4 normal0;
in vec4 worldPos0;

out vec4 fragColor;

uniform sampler2D R_diffuse;
uniform vec2 T_texCoordOffSet;
uniform bool usingSampler;
uniform PointLight R_pointLight;

void main()
{ 
    vec4 totalColor = baseColor * calcPointLight(R_pointLight, normalize(normal0), worldPos0);

    if (usingSampler)
    {
        totalColor = totalColor * texture(R_diffuse, (texCoord0 + T_texCoordOffSet).xy);
    }

    fragColor = totalColor;
}