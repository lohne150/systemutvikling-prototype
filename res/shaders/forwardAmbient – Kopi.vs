#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec2 texCoord0;
out vec3 normal0;

uniform mat4 T_modelMatrix;
uniform mat4 C_viewMatrix;
uniform mat4 T_projectionMatrix;
uniform float R_scalingFactor;
uniform float height;

void main()
{
	normal0 = normalize(normal);

	vec4 position_modelSpace = T_modelMatrix * vec4(position, 1.0);
	position_modelSpace.y = position_modelSpace.y * R_scalingFactor;

	if (normal.y == 1 || normal.y == -1)
	{
		texCoord0 = vec2(texCoord.x, texCoord.y);
	}	
	else
	{
		texCoord0 = vec2(texCoord.x, texCoord.y * height * R_scalingFactor);
	}
	

	gl_Position = T_projectionMatrix * C_viewMatrix * position_modelSpace;
}