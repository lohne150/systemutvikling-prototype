#version 330

layout (location = 0) in vec3 position;

out vec3 texCoord0;

uniform mat4 T_modelMatrix;
uniform mat4 C_viewMatrix;
uniform mat4 T_projectionMatrix;

void main()
{
	texCoord0 = vec3(vec4(position, 1.0));

	gl_Position = T_projectionMatrix * C_viewMatrix * T_modelMatrix * vec4(position, 1.0);
}