#version 330

in vec3 texCoord0;

out vec4 outColor;

uniform samplerCube skybox;
uniform vec4 skyboxColor;

void main()
{
    outColor = texture(skybox, texCoord0) * skyboxColor;
}