#version 330

in vec2 texCoordF;
in vec3 normalF;

out vec4 outColor;

uniform sampler2D diffuse;
uniform float opacity;

void main()
{
	vec3 normal = normalF;
	vec2 texCoord = texCoordF;

	outColor = texture(diffuse, texCoord);
	
	if (outColor.w < 0.1)
	{
		discard;
	}
	
	outColor = outColor * opacity;
}