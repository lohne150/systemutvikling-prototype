#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec2 texCoordF;
out vec3 normalF;

uniform mat4 modelMatrix;

void main()
{
	normalF = normal;
	texCoordF = texCoord;
	gl_Position = modelMatrix * vec4(position.x, position.y, position.z, 1);
}