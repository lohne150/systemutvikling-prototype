#version 330

layout (location = 0) in vec3 position;

uniform mat4 T_MVP;
uniform mat4 T_offSet;

void main()
{
	gl_Position = T_MVP * T_offSet * vec4(position, 1.0);
}