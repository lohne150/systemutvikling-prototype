struct BaseLight
{
    vec4 color;
    float intensity;
};

struct DirectionalLight
{
    BaseLight base;
    vec4 direction;
};

struct Attenuation
{
    float constant;
    float linear;
    float exponent;
};

struct PointLight
{
    BaseLight base;
    Attenuation atten;
    vec4 position;
    float range;
};

struct SpotLight
{
    PointLight pointLight;
    vec4 direction;
    float cutOff;
};

uniform vec4 C_cameraPos;

uniform float specularIntensity;
uniform float specularExponent;

vec4 calcLight(BaseLight base, vec4 direction, vec4 normal, vec4 worldPos)
{
    float diffuseFactor = dot(normalize(normal), -normalize(direction));
    
    vec4 diffuseColor = vec4(0,0,0,0);
    vec4 specularColor = vec4(0,0,0,0);
    
    if(diffuseFactor > 0)
    {
        diffuseColor = base.color * base.intensity * diffuseFactor;
        
        vec4 directionToEye = normalize(C_cameraPos - worldPos);
        vec4 reflectDirection = normalize(reflect(direction, normal));
        
        float specularFactor = dot(directionToEye, reflectDirection);
        specularFactor = pow(specularFactor, specularExponent);
        
        if(specularFactor > 0)
        {
            specularColor = base.color * specularFactor * specularIntensity;
        }
    }
    
    return diffuseColor + specularColor;
}

vec4 calcDirectionalLight(DirectionalLight directionalLight, vec4 normal, vec4 worldPos)
{
    return calcLight(directionalLight.base, -directionalLight.direction, normal, worldPos);
}

vec4 calcPointLight(PointLight pointLight, vec4 normal, vec4 worldPos)
{
    vec4 lightDirection = worldPos - pointLight.position;
    float distanceToLight = length(lightDirection);

    if (distanceToLight > pointLight.range)
    {
        return vec4(0.0);
    }


    lightDirection = normalize(lightDirection);

    vec4 color = calcLight(pointLight.base, lightDirection, normal, worldPos);

    float attenuation = pointLight.atten.constant
                      + pointLight.atten.linear * distanceToLight
                      + pointLight.atten.exponent * distanceToLight * distanceToLight
                      + 0.00001; //Fix division by 0

    return color / attenuation;
}

vec4 calcSpotLight(SpotLight spotLight, vec4 normal, vec4 worldPos)
{
    vec4 lightDirection = normalize(worldPos - spotLight.pointLight.position);
    float spotFactor = dot(lightDirection, normalize(spotLight.direction));

    vec4 color = vec4(0.0);

    if (spotFactor > spotLight.cutOff)
    {
        color = calcPointLight(spotLight.pointLight, normal, worldPos) * (1.0 - (1.0 - spotFactor) / (1.0 - spotLight.cutOff));
    }

    return color;
}