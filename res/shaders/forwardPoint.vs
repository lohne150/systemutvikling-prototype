#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out vec2 texCoord0;
out vec4 normal0;
out vec4 worldPos0;

uniform mat4 T_MVPMatrix;
uniform mat4 T_modelMatrix;
uniform mat4 T_offSet;

void main()
{
	texCoord0 = texCoord;
	normal0 = T_modelMatrix * T_offSet * vec4(normal, 0.0);
	gl_Position = T_MVPMatrix * T_offSet * vec4(position, 1.0);
	worldPos0 = T_modelMatrix * T_offSet * vec4(position, 1.0);
}