#version 330

flat in float visibility0;
in vec2 texCoord0;
in uint textureId0;

out vec4 outColor;

uniform sampler2D sampler;

void main()
{
	outColor = texture(sampler, texCoord0);
	outColor.w = outColor.w * visibility0;

	if (outColor.w < 0.1)
	{
		discard;
	}
}