#version 330

in vec2 texCoord0;
in vec3 normal0;

out vec4 outColor;

uniform sampler2D R_diffuse;
uniform vec4 R_ambient;
uniform float R_scalingFactor;
uniform float height;

void main()
{
	vec2 texCoord = texCoord0;

	if (texCoord.y > height * R_scalingFactor - 0.5 && normal0.y < 0.99)
	{
		texCoord.x += 0.33333333;
		texCoord.y -= int(height * R_scalingFactor * 1000000) % 1000000 / 1000000.0;
	}
	
	outColor = R_ambient * texture(R_diffuse, texCoord.xy);
}