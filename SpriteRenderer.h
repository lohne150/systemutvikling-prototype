#pragma once
#include "Component.h"
#include "Window.h"
#include "OpenGL.h"
#include "Config.h"
#include "Texture.h"
#include "Mesh.h"

namespace tl
{
	namespace components
	{
		class SpriteRenderer : public core::Component
		{
		public:
			SpriteRenderer(Uint32 ownerId) noexcept;
			SpriteRenderer(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~SpriteRenderer() noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			graphics::Mesh m_mesh{ "quad" };
			glm::uvec2 m_size{};
			graphics::Texture m_texture{ "NULL" };
			
		};
	}
}