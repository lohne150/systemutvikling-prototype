#pragma once
#include <vector>
#include <memory>
#include "tinyxml2.h"
#include "observer_ptr.h"
#include "IComponent.h"
#include "Transform.h"

namespace tl
{
	class graphics::Window;

	namespace core
	{
		class Engine;

		class GameObject
		{
		public:
			GameObject() noexcept;
			GameObject(Uint32 parentId) noexcept;
			GameObject(const GameObject& other) noexcept;
			GameObject(tinyxml2::XMLElement* xmlElement) noexcept;
			GameObject(tinyxml2::XMLElement* xmlElement, Uint32 parentId) noexcept;
			~GameObject() noexcept;

			GameObject& operator=(const GameObject& other) noexcept;

			const std::string& getName() const noexcept;
			Transform& getTransform() noexcept;
			Uint32 getParentId() const noexcept;
			Uint32 getId() const noexcept;

			void setName(const std::string& name) noexcept;
			void setParentId(Uint32 parentId) noexcept;

			GameObject clone() noexcept;
			void generateNewId() noexcept;
			void loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept;
			void addComponent(std::unique_ptr<IComponent> component) noexcept;
			void start(graphics::Window& window) noexcept;
			void update(graphics::Window& window, float deltaTime) noexcept;
			void render(graphics::Window& window, graphics::Shader& shader) noexcept;
			
			template <typename T>
			T& getComponent() noexcept;

			static Uint32 getNextGameObjectId() noexcept;
			static void initializeEngine(system::observer_ptr<Engine> engine) noexcept;

		private:
			Uint32 m_id{};
			Uint32 m_parentId{};
			std::string m_name{};
			Transform m_transform;
			std::vector<std::unique_ptr<IComponent>> m_components{};

			static Uint32 s_gameObjectCounter;
			static system::observer_ptr<Engine> s_engine;

		};

		constexpr int a = sizeof(GameObject);
	}
}