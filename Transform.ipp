#pragma once
#include <glm\gtc\matrix_transform.hpp>
#include "Transform.h"

namespace tl
{
	namespace core
	{
		////////////////////////////////////////////////////////////
		inline void Transform::setPosition(const glm::vec3& position) noexcept
		{
			m_position = position;
		}

		////////////////////////////////////////////////////////////
		inline void Transform::setRotation(const glm::quat& rotation) noexcept
		{
			m_rotation = rotation;
		}

		////////////////////////////////////////////////////////////
		inline void Transform::setScale(const glm::vec3& scale) noexcept
		{
			m_scale = scale;
		}

		inline void Transform::setOwnerId(Uint32 ownerId) noexcept
		{
			m_ownerId = ownerId;
		}

		inline void Transform::setParentId(Uint32 parentId) noexcept
		{
			m_parentId = parentId;
		}

		////////////////////////////////////////////////////////////
		inline void Transform::translate(const glm::vec3 & translation) noexcept
		{
			m_position += translation;
		}

		////////////////////////////////////////////////////////////
		inline const glm::vec3 & Transform::getLocalPosition() const noexcept
		{
			return m_position;
		}

		////////////////////////////////////////////////////////////
		inline const glm::quat & Transform::getLocalRotation() const noexcept
		{
			return m_rotation;
		}

		////////////////////////////////////////////////////////////
		inline const glm::vec3 & Transform::getLocalScale() const noexcept
		{
			return m_scale;
		}

		////////////////////////////////////////////////////////////
		inline glm::vec3 Transform::getLocalEulerAngles() const noexcept
		{
			return glm::eulerAngles(m_rotation);
		}

		////////////////////////////////////////////////////////////
		inline glm::mat4 Transform::getLocalModelMatrix() const noexcept
		{
			glm::mat4 result(1.0f);

			result = glm::translate(result, m_position);
			result *= glm::mat4_cast(m_rotation);
			result = glm::scale(result, m_scale);

			return result;
		}

		////////////////////////////////////////////////////////////
		inline glm::vec3 Transform::getLocalForwardDirection() const noexcept
		{
			return glm::vec3(glm::mat4_cast(getLocalRotation()) * glm::vec4(0, 0, 1, 0));
		}

		////////////////////////////////////////////////////////////
		inline glm::vec3 Transform::getLocalUpDirection() const noexcept
		{
			return glm::vec3(glm::mat4_cast(getLocalRotation()) * glm::vec4(0, 1, 0, 0));
		}

		////////////////////////////////////////////////////////////
		inline glm::vec3 Transform::getLocalRightDirection() const noexcept
		{
			return glm::vec3(glm::mat4_cast(getLocalRotation()) * glm::vec4(1, 0, 0, 0));
		}
	}
}

