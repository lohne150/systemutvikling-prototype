#pragma once
#include <string>
#include <vector>

namespace tl
{
	std::vector<std::string> splitString(const std::string& text, char delimiter = ' ') noexcept;
	void removeCharFromString(std::string& text, char character) noexcept;
}