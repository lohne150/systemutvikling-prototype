#include "ParticleEmitter.h"
#include "XMLUtilities.h"
#include <random>
#include "Camera.h"

namespace tl
{
	namespace components
	{
		const std::string ParticleEmitter::type = "ParticleEmitter";
		std::shared_ptr<graphics::ParticleMaster> ParticleEmitter::s_particleMaster;

		ParticleEmitter::ParticleEmitter(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		ParticleEmitter::ParticleEmitter(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			loadFromXML(xmlElement);
		}

		ParticleEmitter::~ParticleEmitter() noexcept
		{
		}

		void ParticleEmitter::setTexture(const graphics::Texture & texture) noexcept
		{
			m_texture = texture;
		}

		void ParticleEmitter::loadFromXML(tinyxml2::XMLElement * xmlElement) noexcept
		{
			if (!xmlElement)
			{
				return;
			}

			tinyxml2::XMLElement* maxParticlesElement = xmlElement->FirstChildElement("maxParticles");
			if (maxParticlesElement)
			{
				maxParticlesElement->QueryUnsignedText(&m_maxParticles);
			}
		}

		glm::vec3 ParticleEmitter::generateRandomUnitVector() noexcept
		{
			std::random_device rd;
			std::mt19937 gen(rd());

			float theta = std::uniform_real_distribution<float>(0, glm::two_pi<float>())(gen);
			float z = std::uniform_real_distribution<float>(-1, 1)(gen);
			float rootOneMinusZSquared = std::sqrt(1.0f - z * z);
			float x = rootOneMinusZSquared * std::cos(theta);
			float y = rootOneMinusZSquared * std::sin(theta);

			return glm::vec3(x, y, z);
		}

		void ParticleEmitter::emitParticle(graphics::Window& window) noexcept
		{
			std::random_device rd;
			std::mt19937 gen(rd());

			graphics::Particle newParticle;
			newParticle.position = s_engine->getGameObject(m_ownerId).getTransform().getPosition();

			glm::vec3 randomUnitVector = generateRandomUnitVector();
			newParticle.velocity = randomUnitVector * std::uniform_real_distribution<float>(m_minForce, m_maxForce)(gen);
			newParticle.timeLeft = m_duration;
			newParticle.totalTime = m_duration;
			newParticle.texture = m_texture;

			s_particleMaster->addParticle(newParticle);
		}

		void ParticleEmitter::initializeParticleMaster(std::shared_ptr<graphics::ParticleMaster> particleMaster) noexcept
		{
			s_particleMaster = particleMaster;
		}

		const std::string& ParticleEmitter::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> ParticleEmitter::clone() const noexcept
		{
			return std::make_unique<ParticleEmitter>(*this);
		}

		void ParticleEmitter::start(graphics::Window& window) noexcept
		{
		}

		void ParticleEmitter::update(graphics::Window& window, float deltaTime) noexcept
		{
			m_sinceLastEmission += deltaTime;
			while (m_sinceLastEmission > 1.0f / m_emissionRate)
			{
				m_sinceLastEmission -= 1.0f / m_emissionRate;
				emitParticle(window);
			}
		}

		void ParticleEmitter::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}