//Trond Lohne 141414

#pragma once
#include <chrono>

class Clock
{
public:
	Clock() noexcept;
	~Clock() noexcept;

	const float restart() noexcept;
	const float elapsedSeconds() noexcept;

private:
	std::chrono::time_point<std::chrono::system_clock> m_start{};
	std::chrono::time_point<std::chrono::system_clock> m_end{};
	std::chrono::duration<float> m_elapsed_seconds{};
};

