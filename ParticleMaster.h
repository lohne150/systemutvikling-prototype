#pragma once
#include <vector>
#include <array>
#include <unordered_map>
#include <atomic>
#include <glm\glm.hpp>
#include <thread>
#include "Texture.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"
#include "Window.h"

namespace tl
{
	namespace graphics
	{
		class ParticleMaster
		{
		public:

			ParticleMaster() noexcept;
			~ParticleMaster() noexcept;

			void initialize() noexcept;
			void update(float deltaTime, Window& window) noexcept;
			void render(Window& window) noexcept;
			void addParticle(const Particle& particle) noexcept;

		private:
			void removeParticle(Uint32 i) noexcept;
			void sortParticlesByDistance() noexcept;
			void drawInstance() noexcept;

		private:
			struct MeshInstancingData
			{
				enum BufferType
				{
					VERTEX_VB,
					TEXTURECOORDINATE_VB,
					INDEX_VB,
					POSITION_VB,
					VISIBILITYV_VB,
					TEXTURE_VB,
					NUM_BUFFERS
				};

				GLuint vao;
				GLuint vbo[NUM_BUFFERS];
				Uint32 drawCount{ 0 };
			} m_meshData;

			std::array<Particle, MAX_PARTICLES> m_sortingParticles;
			std::array<glm::vec3, MAX_PARTICLES> m_positions;
			std::array<glm::vec3, MAX_PARTICLES> m_velocities;
			std::array<float, MAX_PARTICLES> m_timeLefts;
			std::array<float, MAX_PARTICLES> m_totalTimes;
			std::array<float, MAX_PARTICLES> m_visibility;
			std::array<float, MAX_PARTICLES> m_distances;
			std::array<Texture, MAX_PARTICLES> m_textures;
			Uint32 m_numberOfActiveParticles{ 0 };

			Texture m_texture{ "fire" };
			Shader m_shader{ "particleInstantiate" };

		};
	}
}