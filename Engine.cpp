#include <boost\filesystem.hpp>
#include "tinyxml2.h"
#include "OpenGL.h"
#include "Engine.h"
#include "MapKey.h"
#include "Component.h"
#include "observer_ptr.h"
#include "LogManager.h"
#include "TextureUtilities.h"
#include "XMLUtilities.h"
#include "Mesh.h"
#include "MeshUtilities.h"
#include "ParticleEmitter.h"
#include <thread>

namespace tl
{
	namespace core
	{
		Engine::Engine() noexcept
			: m_renderingEngine(system::observer_ptr<Engine>(this))
			, m_window(m_renderingEngine.getWindow())
		{
			m_hierachy.reserve(1000);
		}


		Engine::~Engine() noexcept
		{
		}

		void foo()
		{

		}

		void Engine::initializeEngine() noexcept
		{
			LogManager::restartLog();
			LogManager::log("Initializing core engine\n");

			m_renderingEngine.initializeGraphics();
			InputHandler::initializeWindow(m_window);
			loadSupportedTexturesTypes();
			loadSupportedMeshTypes();

			LogManager::logVersion();
			LogManager::logParams();

			system::observer_ptr<Engine> sharedEngine(this);
			Component::initializeEngine(sharedEngine);
			GameObject::initializeEngine(sharedEngine);
			Transform::initializeEngine(sharedEngine);

			loadTextures();
			loadMeshes();

			m_particleMaster = std::make_shared<graphics::ParticleMaster>();
			components::ParticleEmitter::initializeParticleMaster(m_particleMaster);
			m_renderingEngine.setParticleMaster(m_particleMaster);

			LogManager::log("Core engine initialization done!\n");
		}


		void Engine::loadTextures() noexcept
		{
			tinyxml2::XMLDocument document;
			document.LoadFile(SETTINGS_FILE_PATH);
			std::string textureDirectoryPath = getElementText(document, "Settings/Textures/DirectoryPath");

			LogManager::log("Loading all textures from %s\n", textureDirectoryPath.c_str());

			boost::filesystem::path path(textureDirectoryPath);
			boost::filesystem::directory_iterator end;

			if (boost::filesystem::exists(path))
			{
				for (boost::filesystem::directory_iterator itr(path); itr != end; ++itr)
				{
					std::string fileName = itr->path().filename().string();
					std::string fileType = fileName.substr(fileName.find_first_of('.'));
					fileName = fileName.substr(0, fileName.find_first_of('.'));
					std::string filePath = itr->path().string();

					if (checkTexturesType(fileType))
					{
						graphics::Texture().loadTextureFromFile(fileName, filePath);
					}
				}
			}
			else
			{
				LogManager::logError("Texture directory \"%s\" does not exist!\n", textureDirectoryPath.c_str());
			}

			LogManager::log("Textures loading done!\n");
		}

		void Engine::loadMeshes() noexcept
		{
			tinyxml2::XMLDocument document;
			document.LoadFile(SETTINGS_FILE_PATH);
			std::string meshesDirectoryPath = getElementText(document, "Settings/Meshes/DirectoryPath");

			LogManager::log("Loading all meshes from %s\n", meshesDirectoryPath.c_str());
			
			boost::filesystem::path path(meshesDirectoryPath);
			boost::filesystem::directory_iterator end;

			if (boost::filesystem::exists(path))
			{
				for (boost::filesystem::directory_iterator itr(path); itr != end; ++itr)
				{
					std::string fileName = itr->path().filename().string();
					std::string fileType = fileName.substr(fileName.find_first_of('.'));
					fileName = fileName.substr(0, fileName.find_first_of('.'));
					std::string filePath = itr->path().string();

					if (checkMeshType(fileType))
					{
						graphics::Mesh(fileName, filePath);
					}
				}
			}
			else
			{
				LogManager::logError("Mesh directory \"%s\" does not exist!\n", meshesDirectoryPath.c_str());
			}
		}

		void Engine::createGameObject(tinyxml2::XMLElement* xmlElement, Uint32 parentId) noexcept
		{
			std::string name = xmlElement->Attribute("name");
			m_hierachy.emplace_back(xmlElement, parentId);
			GameObject currentObject = m_hierachy.back();

			tinyxml2::XMLElement* children = xmlElement->FirstChildElement("Children");
			if (children != nullptr)
			{
				tinyxml2::XMLElement* child = children->FirstChildElement("GameObject");

				while (child != nullptr)
				{
					createGameObject(child, currentObject.getId());
					child = child->NextSiblingElement("GameObject");
				}
			}
		}

		void Engine::eventHandling() noexcept
		{
			glfwPollEvents();

			if (glfwWindowShouldClose(m_window->getWindowHandler()))
			{
				m_isActive = false;
			}
			
			m_inputHandler.handleEvents();
		}

		void Engine::update(float deltaTime) noexcept
		{
			for (Uint32 i = 0; i < m_hierachy.size(); ++i)
			{
				m_hierachy.data()[i].update(*m_window, deltaTime);
			}

			m_particleMaster->update(deltaTime, *m_window);
		}

		void Engine::render() noexcept
		{
			m_renderingEngine.render(m_hierachy);
		}

		void Engine::loadScene(const std::string & scenePath) noexcept
		{
			tinyxml2::XMLDocument document;
			document.LoadFile(scenePath.c_str());


			tinyxml2::XMLElement* sceneRoot = document.FirstChildElement("Scene");
			tinyxml2::XMLElement* gameObjects = sceneRoot->FirstChildElement("GameObjects");
			tinyxml2::XMLElement* gameObject = gameObjects->FirstChildElement("GameObject");

			while (gameObject != nullptr)
			{
				createGameObject(gameObject, GameObject::getNextGameObjectId());
				gameObject = gameObject->NextSiblingElement("GameObject");
			}

			for (auto&& object : m_hierachy)
			{
				object.start(*m_window);
			}
		}

		void Engine::stopEngine() noexcept
		{
			m_isActive = false;
		}

		bool Engine::isActive() const noexcept
		{
			return m_isActive;
		}

		void Engine::gameLoop() noexcept
		{
			float deltaTime = m_clock.restart();
			m_secondTimer += deltaTime;
			m_iterationThisSecond++;

			if (m_secondTimer > 1.0f)
			{
				LogManager::log("FPS: %u\n", m_iterationThisSecond);
				m_iterationThisSecond = 0;
				m_secondTimer -= 1.0f;
			}

			eventHandling();
			update(deltaTime);
			render();
		}

		void Engine::addGameObject(core::GameObject& gameObject) noexcept
		{
			bool hasParent = gameObject.getId() != gameObject.getParentId();

			std::string originalName = gameObject.getName();
			std::string newName = originalName;
			Uint32 n = 1;

			while (isGameObjectNameInUse(newName))
			{
				newName = originalName + std::to_string(n++);
			}

			gameObject.setName(newName);

			m_hierachy.push_back(gameObject);
		}

		bool Engine::isGameObjectNameInUse(const std::string & name) const noexcept
		{
			return std::find_if(m_hierachy.begin(), m_hierachy.end(), [&name](const GameObject& object) {return object.getName() == name; }) != m_hierachy.end();
		}

		
		GameObject& Engine::getGameObject(Uint32 gameObjectId) noexcept
		{
			return m_hierachy[gameObjectId];
		}

		GameObject& Engine::getGameObject(const std::string& name) noexcept
		{
			auto result = std::find_if(m_hierachy.begin(), m_hierachy.end(), [&name](const GameObject& object) { return object.getName() == name; });

			if (result == m_hierachy.end())
			{
				LogManager::logError("Gameobject \"%s\" does not exist!\n", name.c_str());
			}

			return *result;
		}
		
		std::shared_ptr<graphics::Window> Engine::getWindow() const noexcept
		{
			return m_window;
		}
	}
}