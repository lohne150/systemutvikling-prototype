//Trond Lohne 141414

#pragma once
#include <vector>
#include <glm\glm.hpp>
#include <unordered_map>
#include "OpenGL.h"
#include "Config.h"

namespace tl
{
	namespace graphics
	{
		class Mesh
		{
		public:
			struct MeshResource
			{
				enum BufferType
				{
					POSITION_VB,
					TEXTURECOORDINATE_VB,
					NORMAL_VB,
					INDEX_VB,
					NUM_BUFFERS
				};

				GLuint vao;
				GLuint vbo[NUM_BUFFERS + 6];
				Uint32 drawCount;

				bool usingDrawElements;
				bool usingTextureCoords;
				bool usingNormals;
			};

			Mesh() noexcept;
			Mesh(const std::string& meshName) noexcept;
			Mesh(const std::string& meshName, Uint32 additionalVbos) noexcept;
			Mesh(const std::string& meshName, const std::string& filePath) noexcept;
			~Mesh() noexcept;

			void addPositions(const std::vector<float>& positions) noexcept;
			void addTextureCoords(const std::vector<float>& textureCoords) noexcept;
			void addNormals(const std::vector<float>& normals) noexcept;
			void addIndices(const std::vector<GLuint>& indices) noexcept;

			const MeshResource& getResource() const noexcept;

			void draw() noexcept;

		private:
			void loadMeshFromFile(const std::string& meshName, const std::string& filePath) noexcept;
			void initMeshData() noexcept;
			void addVbos(Uint32 numberOfVbos) noexcept;

		private:
			MeshResource m_resource;
			std::string m_filePath;

			static std::unordered_map<std::string, MeshResource> s_meshResources;
		};
	}
}
