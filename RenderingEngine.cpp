#include <stdio.h>
#include <stdlib.h>
#include "RenderingEngine.h"
#include "Texture.h"
#include "LogManager.h"
#include "InputHandler.h"
#include "ParticleEmitter.h"

namespace tl
{
	namespace graphics
	{
		////////////////////////////////////////////////////////////
		RenderingEngine::RenderingEngine(system::observer_ptr<core::Engine> engine) noexcept
			: m_window(std::make_shared<Window>(engine))
		{
		}

		////////////////////////////////////////////////////////////
		RenderingEngine::~RenderingEngine() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		void RenderingEngine::setParticleMaster(std::shared_ptr<ParticleMaster> particleMaster) noexcept
		{
			m_particleMaster = particleMaster;
			m_particleMaster->initialize();
		}

		////////////////////////////////////////////////////////////
		void RenderingEngine::initializeGraphics() noexcept
		{
			LogManager::log("Initializing rendering engine\n");

			LogManager::log("Initializing glfw...");
			if (!glfwInit())
			{
				LogManager::logError("Could not initialize GLFW3!\n");
			}
			LogManager::log("done!\n");

			m_window->initialize();

			glewExperimental = GL_TRUE;
			glewInit();
			glfwWindowHint(GLFW_SAMPLES, 4);
			glEnable(GL_DEPTH_TEST);

			shader = Shader("test");

			LogManager::log("Rendering engine done!\n");
		}

		////////////////////////////////////////////////////////////
		std::shared_ptr<Window> RenderingEngine::getWindow() noexcept
		{
			return m_window;
		}

		////////////////////////////////////////////////////////////
		void RenderingEngine::render(std::vector<core::GameObject>& hierachy) noexcept
		{
			m_window->clear(graphics::Color::White);

			m_window->bind();
			shader.bind();

			for (auto&& gameObject : hierachy)
			{
				gameObject.render(*m_window, shader);
			}
			m_particleMaster->render(*m_window);

			m_window->display();
		}
	}
}