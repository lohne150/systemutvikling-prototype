#include "FreeMove.h"
#include "GameObject.h"

namespace tl
{
	namespace components
	{
		const std::string FreeMove::type = "FreeMove";

		FreeMove::FreeMove(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		FreeMove::FreeMove(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			if (xmlElement)
			{
				xmlElement->FirstChildElement("speed")->QueryFloatText(&m_speed);
			}
		}

		FreeMove::FreeMove(Uint32 ownerId, float speed) noexcept
			: Component(type, ownerId)
			, m_speed(speed)
		{
		}

		FreeMove::FreeMove(Uint32 ownerId, float speed, Key upKey, Key downKey, Key leftKey, Key rightKey) noexcept
			: Component(type, ownerId)
			, m_speed(speed)
			, m_upKey(upKey)
			, m_downKey(downKey)
			, m_leftKey(leftKey)
			, m_rightKey(rightKey)
		{
		}

		FreeMove::~FreeMove() noexcept
		{
		}


		const std::string& FreeMove::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> FreeMove::clone() const noexcept
		{
			return std::make_unique<FreeMove>(*this);
		}

		void FreeMove::start(graphics::Window& window) noexcept
		{

		}

		void FreeMove::update(graphics::Window& window, float deltaTime) noexcept
		{
			core::Transform& transform = s_engine->getGameObject(m_ownerId).getTransform();

			//Move forward
			if (InputHandler::isKeyPressed(m_upKey))
			{
				transform.translate(transform.getForwardDirection() * m_speed * deltaTime);
			}
			//Move backwards
			if (InputHandler::isKeyPressed(m_downKey))
			{
				transform.translate(-transform.getForwardDirection() * m_speed * deltaTime);
			}
			//Move to the left
			if (InputHandler::isKeyPressed(m_leftKey))
			{
				transform.translate(transform.getRightDirection() * m_speed * deltaTime);
			}
			//Move to the right
			if (InputHandler::isKeyPressed(m_rightKey))
			{
				transform.translate(-transform.getRightDirection() * m_speed * deltaTime);
			}
		}

		void FreeMove::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}