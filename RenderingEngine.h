#pragma once
#include <memory>
#include "Window.h"
#include "GameObject.h"
#include "Shader.h"
#include "ParticleMaster.h"
#include "Texture.h"

namespace tl
{
	namespace graphics
	{
		class RenderingEngine
		{
		public:
			RenderingEngine(system::observer_ptr<core::Engine> engine) noexcept;
			~RenderingEngine() noexcept;

			void setParticleMaster(std::shared_ptr<ParticleMaster> particleMaster) noexcept;

			std::shared_ptr<Window> getWindow() noexcept;

			void initializeGraphics() noexcept;
			void render(std::vector<core::GameObject>& hierachy) noexcept;

		private:
			Shader shader;
			Texture texture;
			std::shared_ptr<ParticleMaster> m_particleMaster{};
			std::shared_ptr<Window> m_window{};

		};
	}
}