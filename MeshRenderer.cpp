#include "MeshRenderer.h"
#include "Camera.h"

namespace tl
{
	namespace components
	{
		const std::string MeshRenderer::type = "MeshRenderer";

		MeshRenderer::MeshRenderer(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}
		
		MeshRenderer::MeshRenderer(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			loadFromXML(xmlElement);
		}

		MeshRenderer::~MeshRenderer() noexcept
		{
		}

		void MeshRenderer::setMesh(const graphics::Mesh & mesh) noexcept
		{
			m_mesh = mesh;
		}

		void MeshRenderer::loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept
		{
			if (xmlElement)
			{
				tinyxml2::XMLElement* meshElement = xmlElement->FirstChildElement("mesh");

				if (meshElement)
				{
					m_mesh = graphics::Mesh(meshElement->GetText());
				}
			}
		}

		const std::string & MeshRenderer::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> MeshRenderer::clone() const noexcept
		{
			return std::make_unique<MeshRenderer>(*this);
		}

		void MeshRenderer::start(graphics::Window& window) noexcept
		{
		}

		void MeshRenderer::update(graphics::Window& window, float deltaTime) noexcept
		{
		}

		void MeshRenderer::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
			shader.setUniform("modelMatrix", window.getCamera()->getProjectionView() * transform.getModelMatrix());
			m_mesh.draw();
		}
	}
}