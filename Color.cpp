#include "Color.h"
#include <algorithm>

namespace tl
{
	namespace graphics
	{
		////////////////////////////////////////////////////////////
		// Static member data
		////////////////////////////////////////////////////////////
		const Color Color::Black(0, 0, 0);
		const Color Color::White(1, 1, 1);
		const Color Color::Red(1, 0, 0);
		const Color Color::Green(0, 1, 0);
		const Color Color::Blue(0, 0, 1);
		const Color Color::Yellow(1, 1, 0);
		const Color Color::Magenta(1, 0, 1);
		const Color Color::Cyan(0, 1, 1);
		const Color Color::Transparent(0, 0, 0, 0);

		////////////////////////////////////////////////////////////
		Color::Color() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		tl::graphics::Color::Color(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha) noexcept
			: r(red)
			, g(green)
			, b(blue)
			, a(alpha)
		{
		}

		////////////////////////////////////////////////////////////
		Color::~Color() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		bool operator ==(const Color& left, const Color& right)
		{
			return (left.r == right.r) &&
				   (left.g == right.g) &&
				   (left.b == right.b) &&
				   (left.a == right.a);
		}

		////////////////////////////////////////////////////////////
		bool operator !=(const Color& left, const Color& right)
		{
			return !(left == right);
		}

		////////////////////////////////////////////////////////////
		Color operator +(const Color& left, const Color& right)
		{
			return Color(Uint8(std::min(int(left.r) + right.r, 255)),
						 Uint8(std::min(int(left.g) + right.g, 255)),
						 Uint8(std::min(int(left.b) + right.b, 255)),
						 Uint8(std::min(int(left.a) + right.a, 255)));
		}


		////////////////////////////////////////////////////////////
		Color operator -(const Color& left, const Color& right)
		{
			return Color(Uint8(std::max(int(left.r) - right.r, 0)),
						 Uint8(std::max(int(left.g) - right.g, 0)),
						 Uint8(std::max(int(left.b) - right.b, 0)),
						 Uint8(std::max(int(left.a) - right.a, 0)));
		}

		////////////////////////////////////////////////////////////
		Color operator *(const Color& left, const Color& right)
		{
			return Color(Uint8(int(left.r) * right.r / 255),
						 Uint8(int(left.g) * right.g / 255),
						 Uint8(int(left.b) * right.b / 255),
						 Uint8(int(left.a) * right.a / 255));
		}

		////////////////////////////////////////////////////////////
		Color& operator +=(Color& left, const Color& right)
		{
			return left = left + right;
		}

		////////////////////////////////////////////////////////////
		Color& operator -=(Color& left, const Color& right)
		{
			return left = left - right;
		}

		////////////////////////////////////////////////////////////
		Color& operator *=(Color& left, const Color& right)
		{
			return left = left * right;
		}
	}
}