#pragma once
#include <string>
#include <memory>
#include <thread>
#include "GameObject.h"
#include "IncMap.h"
#include "RenderingEngine.h"
#include "Clock.h"
#include "InputHandler.h"
#include "ParticleMaster.h"

namespace tl
{
	namespace core
	{
		class Engine
		{
		public:
			Engine() noexcept;
			~Engine() noexcept;

			void initializeEngine() noexcept;
			void loadScene(const std::string& scenePath) noexcept;
			void stopEngine() noexcept;
			bool isActive() const noexcept;
			void gameLoop() noexcept;
			void addGameObject(core::GameObject& gameObject) noexcept;
			GameObject& getGameObject(Uint32 gameObjectId) noexcept;
			GameObject& getGameObject(const std::string& name) noexcept;
			bool isGameObjectNameInUse(const std::string& name) const noexcept;
			std::shared_ptr<graphics::Window> getWindow() const noexcept;

		private:
			void loadTextures() noexcept;
			void loadMeshes() noexcept;
			void createGameObject(tinyxml2::XMLElement* xmlElement, Uint32 parentId) noexcept;
			void eventHandling() noexcept;
			void update(float deltaTime) noexcept;
			void render() noexcept;

		private:
			std::vector<GameObject> m_hierachy{};
			graphics::RenderingEngine m_renderingEngine;
			std::shared_ptr<graphics::Window> m_window{};
			bool m_isActive{ true };
			Clock m_clock{};
			float m_secondTimer{};
			Uint32 m_iterationThisSecond{};
			InputHandler m_inputHandler{};
			std::shared_ptr<graphics::ParticleMaster> m_particleMaster;

		};
	}
}