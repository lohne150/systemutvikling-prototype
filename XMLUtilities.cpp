#include "XMLUtilities.h"
#include "StringUtils.h"
#include "Config.h"

namespace tl
{
	tinyxml2::XMLElement* getElement(tinyxml2::XMLDocument& document, const std::string& xmlPath) noexcept
	{
		std::vector<std::string> xmlElements = splitString(xmlPath, '/');
		tinyxml2::XMLElement* result = document.FirstChildElement(xmlElements.front().c_str());

		for (Uint32 i = 1; i < xmlElements.size(); ++i)
		{
			if (result == nullptr)
			{
				return nullptr;
			}

			result = result->FirstChildElement(xmlElements[i].c_str());
		}

		return result;
	}

	std::string getElementText(tinyxml2::XMLDocument& document, const std::string& xmlPath) noexcept
	{
		std::vector<std::string> xmlElements = splitString(xmlPath, '/');
		tinyxml2::XMLElement* xmlElement = document.FirstChildElement(xmlElements.front().c_str());

		for (Uint32 i = 1; i < xmlElements.size(); ++i)
		{
			if (xmlElement == nullptr)
			{
				return nullptr;
			}
			
			xmlElement = xmlElement->FirstChildElement(xmlElements[i].c_str());
		}

		return std::string(xmlElement->GetText());
	}

	std::string getElementAttribute(tinyxml2::XMLDocument& document, const std::string& XMLPath, const std::string& attributeName) noexcept
	{
		return std::string();
	}
}