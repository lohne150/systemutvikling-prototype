#include <string>
#include <glm\gtc\matrix_transform.hpp>
#include "Transform.h"
#include "MapKey.h"
#include "Engine.h"

namespace tl
{
	namespace core
	{
		system::observer_ptr<Engine> Transform::s_engine;

		////////////////////////////////////////////////////////////
		Transform::~Transform() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		void Transform::rotate(float angle, const glm::vec3 & axis) noexcept
		{
			glm::quat quat(1, 0, 0, 0);
			quat = glm::rotate(quat, angle, axis);

			m_rotation = quat * m_rotation;
		}

		////////////////////////////////////////////////////////////
		glm::vec3 Transform::getPosition() noexcept
		{
			return glm::vec3(getParentMatrix() * glm::vec4(getLocalPosition(), 1.0f));
		}

		////////////////////////////////////////////////////////////
		glm::quat Transform::getRotation() noexcept
		{
			return glm::quat_cast(getParentMatrix() * glm::mat4_cast(getLocalRotation()));
		}

		////////////////////////////////////////////////////////////
		glm::vec3 Transform::getScale() noexcept
		{
			return glm::vec3(getParentMatrix() * glm::vec4(getLocalScale(), 1.0f));
		}

		////////////////////////////////////////////////////////////
		glm::mat4 Transform::getModelMatrix() noexcept
		{
			return getParentMatrix() * getLocalModelMatrix();
		}

		////////////////////////////////////////////////////////////
		glm::vec3 Transform::getForwardDirection() noexcept
		{
			return glm::vec3(getParentMatrix() * glm::vec4(getLocalForwardDirection(), 0.0f));
		}

		////////////////////////////////////////////////////////////
		glm::vec3 Transform::getUpDirection() noexcept
		{
			return glm::vec3(getParentMatrix() * glm::vec4(getLocalUpDirection(), 0.0f));
		}

		////////////////////////////////////////////////////////////
		glm::vec3 Transform::getRightDirection() noexcept
		{
			return glm::vec3(getParentMatrix() * glm::vec4(getLocalRightDirection(), 0.0f));
		}

		////////////////////////////////////////////////////////////
		const glm::mat4 & Transform::getParentMatrix() noexcept
		{
			if (m_ownerId != m_parentId && s_engine->getGameObject(m_parentId).getTransform().hasChanged())
			{
				m_parentMatrix = s_engine->getGameObject(m_parentId).getTransform().getModelMatrix();
			}

			return m_parentMatrix;
		}

		////////////////////////////////////////////////////////////
		void Transform::loadFromXML(tinyxml2::XMLElement * xmlElement) noexcept
		{
			if (xmlElement)
			{
				xmlElement = xmlElement->FirstChildElement();

				if (xmlElement)
				{
					std::string value = xmlElement->Value();

					if (value == "position_X")
					{
						xmlElement->QueryFloatText(&m_position.x);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "position_Y")
					{
						xmlElement->QueryFloatText(&m_position.y);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "position_Z")
					{
						xmlElement->QueryFloatText(&m_position.z);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}

					glm::vec3 eulerRotation;
					if (value == "rotation_X")
					{
						xmlElement->QueryFloatText(&eulerRotation.x);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "rotation_Y")
					{
						xmlElement->QueryFloatText(&eulerRotation.y);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "rotation_Z")
					{
						xmlElement->QueryFloatText(&eulerRotation.z);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					m_rotation = glm::quat(eulerRotation);

					if (value == "scale_X")
					{
						xmlElement->QueryFloatText(&m_scale.x);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "scale_Y")
					{
						xmlElement->QueryFloatText(&m_scale.y);
						xmlElement = xmlElement->NextSiblingElement();
						value = xmlElement->Value();
					}
					if (value == "scale_Z")
					{
						xmlElement->QueryFloatText(&m_scale.z);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////
		void Transform::update() noexcept
		{
			if (!m_first)
			{
				m_oldPosition = m_position;
				m_oldRotation = m_rotation;
				m_oldScale = m_scale;
			}
			else
			{
				m_first = false;
			}
		}

		////////////////////////////////////////////////////////////
		bool Transform::hasChanged() const noexcept
		{
			if (m_parentId != m_ownerId && s_engine->getGameObject(m_parentId).getTransform().hasChanged())
			{
				return true;
			}
			else if (m_position != m_oldPosition)
			{
				return true;
			}
			else if (m_rotation != m_oldRotation)
			{
				return true;
			}
			else if (m_scale != m_oldScale)
			{
				return true;
			}

			return false;
		}

		////////////////////////////////////////////////////////////
		void Transform::initializeEngine(system::observer_ptr<Engine> engine) noexcept
		{
			s_engine = engine;
		}
	}
}