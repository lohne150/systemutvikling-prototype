//Trond Lohne 141414

#include "Clock.h"


Clock::Clock() noexcept 
	: m_start(std::chrono::system_clock::now())
	, m_end(std::chrono::system_clock::now())
{
}

Clock::~Clock()
{
}

const float Clock::restart() noexcept
{
	float elapsedTime = elapsedSeconds();
	m_start = std::chrono::system_clock::now();

	return elapsedTime;
}

const float Clock::elapsedSeconds() noexcept
{
	m_end = std::chrono::system_clock::now();
	m_elapsed_seconds = m_end - m_start;

	return m_elapsed_seconds.count();
}
