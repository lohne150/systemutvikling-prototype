#include "Component.h"
#include "GameObject.h"
#include "LogManager.h"

namespace tl
{
	namespace core
	{
		system::observer_ptr<Engine> Component::s_engine = nullptr;

		Component::Component(const std::string& componentType, Uint32 ownerId) noexcept
			: m_ownerId(ownerId)
		{
			LogManager::log("Constructing component of type \"%s\" in gameobject with id \"%u\"\n", componentType.c_str(), m_ownerId);
		}

		Component::~Component() noexcept
		{
		}

		void Component::setOwnerId(Uint32 ownerId) noexcept
		{
			m_ownerId = ownerId;
		}

		Uint32 Component::getOwnerId() const noexcept
		{
			return m_ownerId;
		}

		Transform& Component::getTransform() noexcept
		{
			return s_engine->getGameObject(m_ownerId).getTransform();
		}

		void Component::initializeEngine(system::observer_ptr<Engine> engine) noexcept
		{
			LogManager::log("Initializing the components core engine\n");
			s_engine = engine;
		}
	}
}