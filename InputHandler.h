//Trond Lohne 141414

#pragma once
#include <vector>
#include <memory>
#include <glm\glm.hpp>
#include "Window.h"
#include "OpenGL.h"

namespace tl
{ 
	enum class Key
	{
		Unknown =	GLFW_KEY_UNKNOWN,		///< Unhandled key																0
		A =			GLFW_KEY_A,				///< The A key																	1
		B =			GLFW_KEY_B,				///< The B key																	2
		C =			GLFW_KEY_C,				///< The C key																	3
		D =			GLFW_KEY_D,				///< The D key																	4
		E =			GLFW_KEY_E,				///< The E key																	5
		F =			GLFW_KEY_F,				///< The F key																	6
		G =			GLFW_KEY_G,				///< The G key																	7
		H =			GLFW_KEY_H,				///< The H key																	8
		I =			GLFW_KEY_I,				///< The I key																	9
		J =			GLFW_KEY_J,				///< The J key																	10
		K =			GLFW_KEY_K,				///< The K key																	11
		L =			GLFW_KEY_L,				///< The L key																	12
		M =			GLFW_KEY_M,				///< The M key																	13
		N =			GLFW_KEY_N,				///< The N key																	14
		O =			GLFW_KEY_O,				///< The O key																	15
		P =			GLFW_KEY_P,				///< The P key																	16
		Q =			GLFW_KEY_Q,				///< The Q key																	17
		R =			GLFW_KEY_R,				///< The R key																	18
		S =			GLFW_KEY_S,				///< The S key																	19
		T =			GLFW_KEY_T,				///< The T key																	20
		U =			GLFW_KEY_U,				///< The U key																	21
		V =			GLFW_KEY_V,				///< The V key																	22
		W =			GLFW_KEY_W,				///< The W key																	23
		X =			GLFW_KEY_X,				///< The X key																	24
		Y =			GLFW_KEY_Y,				///< The Y key																	25
		Z =			GLFW_KEY_Z,				///< The Z key																	26
		Num0 =		GLFW_KEY_0,				///< The 0 key																	27
		Num1 =		GLFW_KEY_1,				///< The 1 key																	28
		Num2 =		GLFW_KEY_2,				///< The 2 key																	29
		Num3 =		GLFW_KEY_3,				///< The 3 key																	30
		Num4 =		GLFW_KEY_4,				///< The 4 key																	31
		Num5 =		GLFW_KEY_5,				///< The 5 key																	32
		Num6 =		GLFW_KEY_6,				///< The 6 key																	33
		Num7 =		GLFW_KEY_7,				///< The 7 key																	34
		Num8 =		GLFW_KEY_8,				///< The 8 key																	35
		Num9 =		GLFW_KEY_9,				///< The 9 key																	36
		Escape =	GLFW_KEY_ESCAPE,		///< The Escape key																37
		LControl =	GLFW_KEY_LEFT_CONTROL,	///< The left Control key														38
		LShift =	GLFW_KEY_LEFT_SHIFT,	///< The left Shift key															39
		LAlt =		GLFW_KEY_LEFT_ALT,		///< The left Alt key															40
		LSystem =	GLFW_KEY_LEFT_SUPER,	///< The left OS specific key: window (Windows and Linux), apple (MacOS X), ...	41
		RControl =	GLFW_KEY_RIGHT_CONTROL,	///< The right Control key														42
		RShift =	GLFW_KEY_RIGHT_SHIFT,	///< The right Shift key														43
		RAlt =		GLFW_KEY_RIGHT_ALT,		///< The right Alt key															44
		RSystem =	GLFW_KEY_RIGHT_SUPER,	///< The right OS specific key: window (Windows and Linux), apple (MacOS X), ...45
		Menu =		GLFW_KEY_MENU,			///< The Menu key																46
		LBracket =	GLFW_KEY_LEFT_BRACKET,	///< The [ key																	47
		RBracket =	GLFW_KEY_RIGHT_BRACKET,	///< The ] key																	48
		SemiColon = GLFW_KEY_SEMICOLON,		///< The ; key																	49
		Comma =		GLFW_KEY_COMMA,			///< The , key																	50
		Period =	GLFW_KEY_PERIOD,		///< The . key																	51
		Quote =		GLFW_KEY_APOSTROPHE,	///< The ' key																	52
		Slash =		GLFW_KEY_SLASH,			///< The / key																	53
		BackSlash = GLFW_KEY_BACKSLASH,		///< The \ key																	54
		Equal =		GLFW_KEY_EQUAL,			///< The = key																	55
		Dash =		GLFW_KEY_MINUS,			///< The - key																	56
		Space =		GLFW_KEY_SPACE,			///< The Space key																57
		Return =	GLFW_KEY_ENTER,			///< The Return key																58
		BackSpace = GLFW_KEY_BACKSPACE,		///< The Backspace key															59
		Tab =		GLFW_KEY_TAB,			///< The Tabulation key															60
		PageUp =	GLFW_KEY_PAGE_UP,		///< The Page up key															61
		PageDown =	GLFW_KEY_PAGE_DOWN,		///< The Page down key															62
		End =		GLFW_KEY_END,			///< The End key																63
		Home =		GLFW_KEY_HOME,			///< The Home key																64
		Insert =	GLFW_KEY_INSERT,		///< The Insert key																65
		Delete =	GLFW_KEY_DELETE,		///< The Delete key																66
		Add =		GLFW_KEY_KP_ADD,		///< The + key																	67
		Subtract =	GLFW_KEY_KP_SUBTRACT,	///< The - key																	68
		Multiply =	GLFW_KEY_KP_MULTIPLY,	///< The * key																	69
		Divide =	GLFW_KEY_KP_DIVIDE,		///< The / key																	70
		Left =		GLFW_KEY_LEFT,			///< Left arrow																	71
		Right =		GLFW_KEY_RIGHT,			///< Right arrow																72
		Up =		GLFW_KEY_UP,			///< Up arrow																	73
		Down =		GLFW_KEY_DOWN,			///< Down arrow																	74
		Numpad0 =	GLFW_KEY_KP_0,			///< The numpad 0 key															75
		Numpad1 =	GLFW_KEY_KP_1,			///< The numpad 1 key															76
		Numpad2 =	GLFW_KEY_KP_2,			///< The numpad 2 key															77
		Numpad3 =	GLFW_KEY_KP_3,			///< The numpad 3 key															78
		Numpad4 =	GLFW_KEY_KP_4,			///< The numpad 4 key															79
		Numpad5 =	GLFW_KEY_KP_5,			///< The numpad 5 key															80
		Numpad6 =	GLFW_KEY_KP_6,			///< The numpad 6 key															81
		Numpad7 =	GLFW_KEY_KP_7,			///< The numpad 7 key															82
		Numpad8 =	GLFW_KEY_KP_8,			///< The numpad 8 key															83
		Numpad9 =	GLFW_KEY_KP_9,			///< The numpad 9 key															84
		F1 =		GLFW_KEY_F1,			///< The F1 key																	85
		F2 =		GLFW_KEY_F2,			///< The F2 key																	86
		F3 =		GLFW_KEY_F3,			///< The F3 key																	87
		F4 =		GLFW_KEY_F4,			///< The F4 key																	88
		F5 =		GLFW_KEY_F5,			///< The F5 key																	89
		F6 =		GLFW_KEY_F6,			///< The F6 key																	90
		F7 =		GLFW_KEY_F7,			///< The F7 key																	91
		F8 =		GLFW_KEY_F8,			///< The F8 key																	92
		F9 =		GLFW_KEY_F9,			///< The F9 key																	93
		F10 =		GLFW_KEY_F10,			///< The F10 key																94
		F11 =		GLFW_KEY_F11,			///< The F11 key																95
		F12 =		GLFW_KEY_F12,			///< The F12 key																96
		F13 =		GLFW_KEY_F13,			///< The F13 key																97
		F14 =		GLFW_KEY_F14,			///< The F14 key																98
		F15 =		GLFW_KEY_F15,			///< The F15 key																99
		Pause =		GLFW_KEY_PAUSE,			///< The Pause key																100

		LastKey = GLFW_KEY_LAST,				///<																		111
		KeyCount							///< Keep last -- the total number of keyboard keys								112
	};

	enum class Button
	{
		Mouse1 = GLFW_MOUSE_BUTTON_1,			///< The 1 mouse button															0
		Mouse2 = GLFW_MOUSE_BUTTON_2,			///< The 2 mouse button															1
		Mouse3 = GLFW_MOUSE_BUTTON_3,			///< The 3 mouse button															2
		Mouse4 = GLFW_MOUSE_BUTTON_4,			///< The 4 mouse button															3
		Mouse5 = GLFW_MOUSE_BUTTON_5,			///< The 5 mouse button															4
		Mouse6 = GLFW_MOUSE_BUTTON_6,			///< The 6 mouse button															5
		Mouse7 = GLFW_MOUSE_BUTTON_7,			///< The 7 mouse button															6
		Mouse8 = GLFW_MOUSE_BUTTON_8,			///< The 8 mouse button															7
		LMouse = GLFW_MOUSE_BUTTON_LEFT,		///< The left mouse button														8
		RMouse = GLFW_MOUSE_BUTTON_RIGHT,		///< The right mouse button														9
		MMouse = GLFW_MOUSE_BUTTON_MIDDLE,		///< The middle mouse button													10

		LastButton = GLFW_MOUSE_BUTTON_LAST,	///<																			11
		ButtonCount								///< Keep last -- The total number of mouse buttons								12
	};

	class InputHandler
	{
	public:

		InputHandler() noexcept;
		~InputHandler() noexcept;

		void handleEvents() noexcept;

		static bool isKeyPressed(Key keyCode) noexcept;
		static bool isKeyUp(Key keyCode) noexcept;
		static bool isKeyDown(Key keyCode) noexcept;

		static bool isButtonPressed(Button buttonCode) noexcept;
		static bool isButtonUp(Button buttonCode) noexcept;
		static bool isButtonDown(Button buttonCode) noexcept;

		static glm::vec2 getMousePosition() noexcept;
		static glm::vec2 getMouseDelta() noexcept;

		static void initializeWindow(std::shared_ptr<graphics::Window> window) noexcept;

	private:
		void handleMouseInput() noexcept;

	private:
		std::vector<Key> m_activeKeys;
		std::vector<Button> m_activeButtons;

		static glm::dvec2 s_mousePosition;
		static glm::dvec2 s_oldMousePosition;
		static std::vector<bool> s_currentKeys;
		static std::vector<bool> s_upKeys;
		static std::vector<bool> s_downKeys;
		static std::vector<bool> s_currentButtons;
		static std::vector<bool> s_upButtons;
		static std::vector<bool> s_downButtons;
		static std::shared_ptr<graphics::Window> s_window;

	};
}