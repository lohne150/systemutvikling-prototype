//Trond Lohne 141414

#pragma once
#include <string>
#include <memory>
#include <unordered_map>
#include <glm\glm.hpp>
#include "OpenGL.h"
#include "Transform.h"

class RenderingEngine;

namespace tl
{
	namespace graphics
	{
		class Shader
		{
		public:
			Shader() noexcept;
			Shader(const std::string& shaderName) noexcept;
			~Shader() noexcept;

			void addVertexShaderFromFile(const std::string& filePath) noexcept;
			void addFragmentShaderFromFile(const std::string& filePath) noexcept;
			void addGeometryShaderFromFile(const std::string& filePath) noexcept;
			void addVertexShader(const std::string& text) noexcept;
			void addFragmentShader(const std::string& text) noexcept;
			void addGeometryShader(const std::string& text) noexcept;
			void addProgram(const std::string& text, int type) noexcept;

			void compileShader() noexcept;
			void bind() const noexcept;

			std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> findUniformStructs(const std::string& shaderProgram) noexcept;
			void addAllUniforms(std::string& shaderProgram) noexcept;
			void addUniform(const std::string& uniformName, const std::string& uniformType, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& structs) noexcept;

			void setUniform(const std::string& uniformName, GLuint value) noexcept;
			void setUniform(const std::string& uniformName, GLint value) noexcept;
			void setUniform(const std::string& uniformName, GLfloat value) noexcept;
			void setUniform(const std::string& uniformName, const glm::vec2& value) noexcept;
			void setUniform(const std::string& uniformName, const glm::vec3& value) noexcept;
			void setUniform(const std::string& uniformName, const glm::vec4& value) noexcept;
			void setUniform(const std::string& uniformName, const glm::mat4& value) noexcept;

		private:
			std::string findStructName(const std::string& shaderProgram, size_t structStart) noexcept;
			std::string findStructContent(const std::string& shaderProgram, size_t structStart) noexcept;
			std::string loadShaderFromFile(const std::string& filePath) noexcept;

		private:
			struct ShaderResource
			{
				GLuint program;
				std::unordered_map<std::string, GLuint> uniforms;
				std::vector<std::pair<std::string, std::string>> uniformTypeAndNames;
			};

			ShaderResource m_resource;

			static std::unordered_map<std::string, ShaderResource> s_shaderResources;
		};
	}
}