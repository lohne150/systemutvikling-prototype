#include "Texture.h"
#include "Image.h"
#include "LogManager.h"

namespace tl
{
	namespace graphics
	{
		std::unordered_map<std::string, Texture::TextureData> Texture::s_textures;


		////////////////////////////////////////////////////////////
		Texture::Texture() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		Texture::Texture(const std::string& textureName) noexcept
		{
			if (s_textures.count(textureName) == 1)
			{
				m_data = s_textures[textureName];
			}
			else
			{
				LogManager::logError("Failed to find texture \"%s\"", textureName.c_str());
			}
		}

		////////////////////////////////////////////////////////////
		Texture::Texture(const std::string & textureName, const Image& image) noexcept
		{
			if (s_textures.count(textureName) == 1 || !createTexture(textureName, image))
			{
				LogManager::logError("Failed to create new texture \"%s\" with image\n", textureName.c_str());
			}
		}

		////////////////////////////////////////////////////////////
		Texture::~Texture() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		Texture::TextureData Texture::getData() const noexcept
		{
			return m_data;
		}

		////////////////////////////////////////////////////////////
		void Texture::bind(Uint32 textureSlot) noexcept
		{
			glActiveTexture(textureSlot);
			glBindTexture(GL_TEXTURE_2D, m_data.textureId);
		}

		////////////////////////////////////////////////////////////
		void Texture::loadTextureFromFile(const std::string& textureName, const std::string& textureFilePath) noexcept
		{
			//Makes sure that this texture isn't already loaded, if so returned the already loaded texture
			if (s_textures.count(textureName) == 1)
			{
				m_data = s_textures[textureName];
				return;
			}

			LogManager::log("Creating texture \"%s\"\n", textureName.c_str());
			Image image;

			if (!image.loadImageFromFile(textureFilePath) || !createTexture(textureName, image))
			{
				LogManager::log("FAILED TO LOAD TEXTURE!\n");
			}
		}
		bool Texture::createTexture(const std::string& textureName, const Image& image) noexcept
		{
			const Uint8* pixels = image.getPixelsPtr();

			//Checks if the pixels were loaded properly
			if (pixels)
			{
				//Generate and bind buffer
				GLuint textureBuffer = 0;
				glGenTextures(1, &textureBuffer);
				glBindTexture(GL_TEXTURE_2D, textureBuffer);

				//Tell openGL what to do when the texture wraps
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

				//Create the actual texture
				glm::vec2 size = image.getSize();
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, static_cast<Uint32>(size.x), static_cast<Uint32>(size.y), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());

				//Tell openGL what to do when it renders the texture at a higher or lower resolution
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
				glGenerateMipmap(GL_TEXTURE_2D);

				//Unbind texture
				glBindTexture(GL_TEXTURE_2D, 0);

				m_data.textureId = textureBuffer;
				s_textures.emplace(textureName, m_data);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}