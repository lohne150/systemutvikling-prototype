#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class FreeMove : public core::Component
		{
		public:
			FreeMove(Uint32 ownerId) noexcept;
			FreeMove(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			FreeMove(Uint32 ownerId, float speed) noexcept;
			FreeMove(Uint32 ownerId, float speed, Key upKey, Key downKey, Key leftKey, Key rightKey) noexcept;
			virtual ~FreeMove() noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			float m_speed{ 1.0f };
			Key m_upKey{ Key::W };
			Key m_downKey{ Key::S };
			Key m_leftKey{ Key::A };
			Key m_rightKey{ Key::D };

		};

	}
}
