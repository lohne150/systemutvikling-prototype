#include "FreeLook.h"
#include "InputHandler.h"
#include "GameObject.h"


namespace tl
{
	namespace components
	{
		const std::string FreeLook::type = "FreeLook";
		const float FreeLook::c_upLimit = 0.9f;
		const float FreeLook::c_downLimit = -0.9f;

		FreeLook::FreeLook(Uint32 ownerId) noexcept
			: Component(type, ownerId)
			, m_speed(glm::pi<float>())
		{
		}

		FreeLook::FreeLook(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			if (xmlElement)
			{
				xmlElement->FirstChildElement("speed")->QueryFloatText(&m_speed);
			}
		}

		FreeLook::FreeLook(Uint32 ownerId, float speed) noexcept
			: Component(type, ownerId)
			, m_speed(speed)
		{
		}

		FreeLook::FreeLook(Uint32 ownerId, float speed, Key upKey, Key downKey, Key leftKey, Key rightKey) noexcept
			: Component(type, ownerId)
			, m_speed(speed)
			, m_upKey(upKey)
			, m_downKey(downKey)
			, m_leftKey(leftKey)
			, m_rightKey(rightKey)
		{
		}


		FreeLook::~FreeLook()
		{
		}

		const std::string& FreeLook::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> FreeLook::clone() const noexcept
		{
			return std::make_unique<FreeLook>(*this);
		}

		void FreeLook::start(graphics::Window& window) noexcept
		{
			window.setInputMode(GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}

		void FreeLook::update(graphics::Window& window, float deltaTime) noexcept
		{
			core::Transform& transform = s_engine->getGameObject(m_ownerId).getTransform();

			//Look up
			if (InputHandler::isKeyPressed(m_upKey) && transform.getLocalForwardDirection().y < c_upLimit)
			{
				transform.rotate(-m_speed * deltaTime, transform.getRightDirection());
			}
			//Look down
			if (InputHandler::isKeyPressed(m_downKey) && transform.getLocalForwardDirection().y > c_downLimit)
			{
				transform.rotate(m_speed * deltaTime, transform.getRightDirection());
			}
			//Look left
			if (InputHandler::isKeyPressed(m_leftKey))
			{
				transform.rotate(m_speed * deltaTime, glm::vec3(0, 1, 0));
			}
			//Look right
			if (InputHandler::isKeyPressed(m_rightKey))
			{
				transform.rotate(-m_speed * deltaTime, glm::vec3(0, 1, 0));
			}
		}

		void FreeLook::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}