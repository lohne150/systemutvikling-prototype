#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class MouseLook : public core::Component
		{
		public:
			MouseLook(Uint32 ownerId) noexcept;
			MouseLook(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			MouseLook(Uint32 ownerId, float speed) noexcept;
			virtual ~MouseLook();

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:

			float m_speed{ 0.1f };
			float m_upperLookLimit{ 0.9f };
			float m_lowerLookLimit{ 0.1f };

		};
	}
}

