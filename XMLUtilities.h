#pragma once
#include <string>
#include "tinyxml2.h"

namespace tl
{
	tinyxml2::XMLElement* getElement(tinyxml2::XMLDocument& document, const std::string& xmlPath) noexcept;
	std::string getElementText(tinyxml2::XMLDocument& document, const std::string& xmlPath) noexcept;
	std::string getElementAttribute(tinyxml2::XMLDocument& document, const std::string& xmlPath, const std::string& attributeName) noexcept;
}