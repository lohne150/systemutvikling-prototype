#include <fstream>
#include <regex>
#include "Mesh.h"
#include "StringUtils.h"
#include "LogManager.h"
#include "tiny_obj_loader.h"

const GLuint c_positionLayoutId = 0;
const GLuint c_texCoordLayoutId = 1;
const GLuint c_normalLayoutId = 2;

namespace tl
{
	namespace graphics
	{
		std::unordered_map<std::string, Mesh::MeshResource> Mesh::s_meshResources;

		Mesh::Mesh() noexcept
		{
			initMeshData();
		}

		Mesh::Mesh(const std::string& meshName) noexcept
		{
			if (s_meshResources.count(meshName) == 1)
			{
				m_resource = s_meshResources[meshName];
			}
			else
			{
				LogManager::logError("Failed to find mesh \"%s\"\n", meshName.c_str());
			}
		}

		Mesh::Mesh(const std::string& meshName, Uint32 additionalVbos) noexcept
		{
			if (s_meshResources.count(meshName) == 1)
			{
				m_resource = s_meshResources[meshName];
			}
			else
			{
				LogManager::logError("Failed to find mesh \"%s\"\n", meshName.c_str());
			}
		}

		Mesh::Mesh(const std::string& meshName, const std::string & filePath) noexcept
		{
			m_filePath = filePath;

			//If the mesh has already been loaded, use the already existing mesh data
			if (s_meshResources.count(meshName) == 1)
			{
				m_resource = s_meshResources[meshName];
			}
			else
			{
				initMeshData();
				loadMeshFromFile(meshName, filePath);
			}
		}


		Mesh::~Mesh() noexcept
		{
		}

		void Mesh::addPositions(const std::vector<float>& positions) noexcept
		{
			m_resource.drawCount = positions.size() / 3;

			glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::POSITION_VB]);
			glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(positions.front()), &positions.front(), GL_STATIC_DRAW);
		}

		void Mesh::addTextureCoords(const std::vector<float>& textureCoords) noexcept
		{
			m_resource.usingTextureCoords = true;

			glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::TEXTURECOORDINATE_VB]);
			glBufferData(GL_ARRAY_BUFFER, textureCoords.size() * sizeof(textureCoords.front()), &textureCoords.front(), GL_STATIC_DRAW);
		}

		void Mesh::addNormals(const std::vector<float>& normals) noexcept
		{
			m_resource.usingNormals = true;

			glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::NORMAL_VB]);
			glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(normals.front()), &normals[0], GL_STATIC_DRAW);
		}

		void Mesh::addIndices(const std::vector<GLuint>& indices) noexcept
		{
			m_resource.usingDrawElements = true;
			m_resource.drawCount = indices.size();

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_resource.vbo[MeshResource::INDEX_VB]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices.front()), &indices.front(), GL_STATIC_DRAW);
		}

		const Mesh::MeshResource& Mesh::getResource() const noexcept
		{
			return m_resource;
		}

		void Mesh::loadMeshFromFile(const std::string& meshName, const std::string& filePath) noexcept
		{
			std::string fileType = splitString(filePath, '.').back();
			if (fileType != "obj")
			{
				LogManager::logError("File \"%s\" not supported for mesh data!", fileType.c_str());
			}

			std::vector<tinyobj::shape_t> shapes;
			std::vector<tinyobj::material_t> materials;
			std::string error;

			bool result = tinyobj::LoadObj(shapes, materials, error, filePath.c_str());

			if (!result)
			{
				LogManager::logError("%s", error.c_str());
			}

			if (!shapes.front().mesh.positions.empty())
			{
				addPositions(shapes.front().mesh.positions);
			}

			if (!shapes.front().mesh.texcoords.empty())
			{
				addTextureCoords(shapes.front().mesh.texcoords);
			}

			if (!shapes.front().mesh.normals.empty())
			{
				addNormals(shapes.front().mesh.normals);
			}

			if (!shapes.front().mesh.indices.empty())
			{
				addIndices(shapes.front().mesh.indices);
			}

			s_meshResources.emplace(meshName, m_resource);
		}

		void Mesh::addVbos(Uint32 numberOfVbos) noexcept
		{
			glBindVertexArray(m_resource.vao);
			glGenBuffers(numberOfVbos, &m_resource.vbo[MeshResource::NUM_BUFFERS]);
		}

		void Mesh::draw() noexcept
		{
			glEnableVertexAttribArray(c_positionLayoutId);
			glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::POSITION_VB]);
			glVertexAttribPointer(c_positionLayoutId, 3, GL_FLOAT, GL_FALSE, 0, 0);

			if (m_resource.usingTextureCoords)
			{
				glEnableVertexAttribArray(c_texCoordLayoutId);
				glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::TEXTURECOORDINATE_VB]);
				glVertexAttribPointer(c_texCoordLayoutId, 2, GL_FLOAT, GL_FALSE, 0, 0);
			}

			if (m_resource.usingNormals)
			{
				glEnableVertexAttribArray(c_normalLayoutId);
				glBindBuffer(GL_ARRAY_BUFFER, m_resource.vbo[MeshResource::NORMAL_VB]);
				glVertexAttribPointer(c_normalLayoutId, 3, GL_FLOAT, GL_FALSE, 0, 0);
			}

			if (m_resource.usingDrawElements)
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_resource.vbo[MeshResource::INDEX_VB]);
				glDrawElements(GL_TRIANGLES, m_resource.drawCount, GL_UNSIGNED_INT, NULL);
			}
			else
			{
				glDrawArrays(GL_TRIANGLES, 0, m_resource.drawCount);
			}

			glDisableVertexAttribArray(c_positionLayoutId);

			if (m_resource.usingTextureCoords)
			{
				glDisableVertexAttribArray(c_texCoordLayoutId);
			}

			if (m_resource.usingNormals)
			{
				glDisableVertexAttribArray(c_normalLayoutId);
			}
		}

		void Mesh::initMeshData() noexcept
		{
			m_resource.drawCount = 0;
			m_resource.usingDrawElements = false;
			m_resource.usingTextureCoords = false;
			m_resource.usingNormals = false;

			glGenVertexArrays(1, &m_resource.vao);
			glBindVertexArray(m_resource.vao);

			glGenBuffers(MeshResource::NUM_BUFFERS, m_resource.vbo);
		}
	}
}