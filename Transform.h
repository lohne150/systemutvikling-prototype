#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include "tinyxml2.h"
#include "observer_ptr.h"
#include "Config.h"

namespace tl
{
	namespace core
	{
		class Engine;

		class Transform
		{
		public:
			Transform()
			{
				m_ownerId = 0;
				m_parentId = 0;
			}
			Transform(Uint32 ownerId) noexcept
			{
				m_ownerId = ownerId;
			}
			Transform(Uint32 ownerId, tinyxml2::XMLElement* xmlElement) noexcept
			{
				loadFromXML(xmlElement);
				setOwnerId(ownerId);
			}
			~Transform() noexcept;

			inline void setPosition(const glm::vec3& position) noexcept;
			inline void setRotation(const glm::quat& rotation) noexcept;
			inline void setScale(const glm::vec3& scale) noexcept;
			inline void setOwnerId(Uint32 ownerId) noexcept;
			inline void setParentId(Uint32 parentId) noexcept;
			inline void translate(const glm::vec3& translation) noexcept;
			void rotate(float angle, const glm::vec3& axis) noexcept;

			inline const glm::vec3& getLocalPosition() const noexcept;
			inline const glm::quat& getLocalRotation() const noexcept;
			inline const glm::vec3& getLocalScale() const noexcept;
			inline glm::vec3 getLocalEulerAngles() const noexcept;
			inline glm::mat4 getLocalModelMatrix() const noexcept;
			inline glm::vec3 getLocalForwardDirection() const noexcept;
			inline glm::vec3 getLocalUpDirection() const noexcept;
			inline glm::vec3 getLocalRightDirection() const noexcept;

			glm::vec3 getPosition() noexcept;
			glm::quat getRotation() noexcept;
			glm::vec3 getScale() noexcept;
			glm::mat4 getModelMatrix() noexcept;
			glm::vec3 getForwardDirection() noexcept;
			glm::vec3 getUpDirection() noexcept;
			glm::vec3 getRightDirection() noexcept;

			const glm::mat4& getParentMatrix() noexcept;

			void loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept;
			void update() noexcept;
			bool hasChanged() const noexcept;
			
			static void initializeEngine(system::observer_ptr<Engine> engine) noexcept;

		private:
			glm::vec3 m_position{ 0, 0, 0 };
			glm::quat m_rotation{ 0, 0, 0, 1 };
			glm::vec3 m_scale{ 1, 1, 1 };

			glm::vec3 m_oldPosition{ 0, 0, 0 };
			glm::quat m_oldRotation{ 0, 0, 0, 1 };
			glm::vec3 m_oldScale{ 1, 1, 1 };

			Uint32 m_ownerId;
			Uint32 m_parentId;
			glm::mat4 m_parentMatrix{ 1.0f };
			bool m_first{ true };

			static system::observer_ptr<Engine> s_engine;
		};
	}
}

#include "Transform.ipp"