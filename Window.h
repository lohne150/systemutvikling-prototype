#pragma once
#include <string>
#include <glm\glm.hpp>
#include "OpenGL.h"
#include "Color.h"
#include "observer_ptr.h"
#include "MapKey.h"

namespace tl
{
	namespace components
	{
		class Camera;
	}

	namespace core
	{
		class Engine;
	}

	namespace graphics
	{
		class Window
		{
		public:
			Window(system::observer_ptr<core::Engine> engine) noexcept;
			~Window() noexcept;
			
			void setCameraId(Uint32 cameraId) noexcept;
			void setTitle(const std::string& title) noexcept;
			void setInputMode(Int32 mode, Int32 value) noexcept;
			
			system::observer_ptr<components::Camera> getCamera() const noexcept;
			const glm::ivec2& getSize() const noexcept;
			GLFWwindow* getWindowHandler() noexcept;
			Int32 getInputMode(Int32 mode) noexcept;


			void initialize() noexcept;
			void clear(const Color& color = Color::Black) noexcept;
			void bind() noexcept;
			void display() noexcept;

		private:
			GLFWwindow* m_windowHandle{};
			glm::ivec2 m_size{ 600, 600 };
			std::string m_title{ "Untitled Window" };
			Uint32 m_cameraId{};
			system::observer_ptr<core::Engine> m_engine{};

		};
	}
}
