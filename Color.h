#pragma once
#include "Config.h"

namespace tl
{
	namespace graphics
	{
		class Color
		{
		public:
			Color() noexcept;
			Color(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha = 1) noexcept;
			~Color() noexcept;


		public:
			Uint8 r{};
			Uint8 g{};
			Uint8 b{};
			Uint8 a{};

			static const Color Black;
			static const Color White;
			static const Color Red;
			static const Color Green;
			static const Color Blue;
			static const Color Yellow;
			static const Color Magenta;
			static const Color Cyan;
			static const Color Transparent;
		};

		bool operator ==(const Color& left, const Color& right);
		bool operator !=(const Color& left, const Color& right);
		Color operator +(const Color& left, const Color& right);
		Color operator -(const Color& left, const Color& right);
		Color operator *(const Color& left, const Color& right);
		Color& operator +=(Color& left, const Color& right);
		Color& operator -=(Color& left, const Color& right);
		Color& operator *=(Color& left, const Color& right);
	}
}