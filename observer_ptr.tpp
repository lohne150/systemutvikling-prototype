#pragma once
#include "observer_ptr.h"

namespace tl
{
	namespace system
	{
		template<typename T>
		observer_ptr<T>::observer_ptr() noexcept
		{
		}
		template<typename T>
		observer_ptr<T>::observer_ptr(T* ptr) noexcept
			: m_data(ptr)
		{
		}
		template<typename T>
		observer_ptr<T>::observer_ptr(const observer_ptr & other) noexcept
		{
			*this = other;
		}

		template<typename T>
		observer_ptr<T>::~observer_ptr() noexcept
		{
		}

		template<typename T>
		observer_ptr<T> & observer_ptr<T>::operator=(const observer_ptr<T>& other) noexcept
		{
			m_data = other.m_data;
			return *this;
		}

		template<typename T>
		T& observer_ptr<T>::operator*() const noexcept
		{
			return *m_data;
		}
		template<typename T>
		T* observer_ptr<T>::operator->() const noexcept
		{
			return m_data;
		}

		template<typename T>
		T* observer_ptr<T>::get() const noexcept
		{
			return m_data;
		}

		template<typename T>
		bool operator ==(const observer_ptr<T>& lhs, std::nullptr_t rhs) noexcept
		{
			return lhs.get() == rhs;
		}

		template<typename T>
		bool operator ==(std::nullptr_t lhs, const observer_ptr<T>& rhs) noexcept
		{
			return rhs == lhs;
		}

		template<typename T>
		bool operator !=(const observer_ptr<T>& lhs, std::nullptr_t rhs) noexcept
		{
			return !(lhs == rhs);
		}

		template<typename T>
		bool operator !=(std::nullptr_t lhs, const observer_ptr<T>& rhs) noexcept
		{
			return rhs != lhs;
		}
	}
}