#include <stdio.h>
#include <stdlib.h>
#include "tinyxml2.h"
#include "Window.h"
#include "GLFWCallbackFunctions.h"
#include "Camera.h"
#include "observer_ptr.h"
#include "LogManager.h"
#include "XMLUtilities.h"
#include "Engine.h"

namespace tl
{
	namespace graphics
	{
		////////////////////////////////////////////////////////////
		Window::Window(system::observer_ptr<core::Engine> engine) noexcept
			: m_engine(engine)
		{
			tinyxml2::XMLDocument document;
			document.LoadFile(SETTINGS_FILE_PATH);

			tinyxml2::XMLElement* windowElement = getElement(document, "Settings/Window");
			tinyxml2::XMLElement* widthElement = windowElement->FirstChildElement("Width");
			tinyxml2::XMLElement* heightElement = windowElement->FirstChildElement("Height");
			tinyxml2::XMLElement* titleElement = windowElement->FirstChildElement("Title");

			if (widthElement)
			{
				widthElement->QueryIntText(&m_size.x);
			}
			if (widthElement)
			{
				heightElement->QueryIntText(&m_size.y);
			}
			if (widthElement)
			{
				m_title = titleElement->GetText();
			}
		}

		////////////////////////////////////////////////////////////
		Window::~Window() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		void Window::setCameraId(Uint32 cameraId) noexcept
		{
			m_cameraId = cameraId;
		}

		////////////////////////////////////////////////////////////
		void Window::setTitle(const std::string & title) noexcept
		{
			glfwSetWindowTitle(m_windowHandle, title.c_str());
			m_title = title;
		}

		void Window::setInputMode(Int32 mode, Int32 value) noexcept
		{
			glfwSetInputMode(m_windowHandle, mode, value);
		}

		////////////////////////////////////////////////////////////
		void Window::initialize() noexcept
		{
			LogManager::log("Creating window...");
			m_windowHandle = glfwCreateWindow(static_cast<int>(m_size.x), static_cast<int>(m_size.y), m_title.c_str(), NULL, NULL);

			if (!m_windowHandle)
			{
				glfwTerminate();
				LogManager::logError("Could not open window with GLFW3!\n");
			}
			LogManager::log("done!\n");

			glfwMakeContextCurrent(m_windowHandle);
			glfwSetWindowCloseCallback(m_windowHandle, windowCloseCallback);
		}

		////////////////////////////////////////////////////////////
		void Window::clear(const Color & color) noexcept
		{
			glClearColor(color.r, color.g, color.b, color.a);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		///// \( O_0)/ ////////////////////////////////////////////
		void Window::bind() noexcept
		{
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glfwGetWindowSize(m_windowHandle, &m_size.x, &m_size.y);
			glViewport(0, 0, m_size.x, m_size.y);
		}

		////////////////////////////////////////////////////////////
		void Window::display() noexcept
		{
			glfwSwapBuffers(m_windowHandle);

			
		}

		////////////////////////////////////////////////////////////
		GLFWwindow* Window::getWindowHandler() noexcept
		{
			return m_windowHandle;
		}

		Int32 Window::getInputMode(Int32 mode) noexcept
		{
			return glfwGetInputMode(m_windowHandle, mode);
		}

		////////////////////////////////////////////////////////////
		system::observer_ptr<components::Camera> Window::getCamera() const noexcept
		{

			return system::observer_ptr<components::Camera>(&m_engine->getGameObject(m_cameraId).getComponent<components::Camera>());
		}

		////////////////////////////////////////////////////////////
		const glm::ivec2& Window::getSize() const noexcept
		{
			return m_size;
		}
	}
}