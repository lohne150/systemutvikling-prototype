#pragma once

namespace tl
{
	namespace system
	{
		template <typename T>
		class observer_ptr
		{
		public:
			observer_ptr() noexcept;
			observer_ptr(T* ptr) noexcept;
			observer_ptr(const observer_ptr& other) noexcept;
			~observer_ptr() noexcept;

			observer_ptr& operator =(const observer_ptr& other) noexcept;
			void operator delete(void* ptr) = delete;
			T& operator *() const noexcept;
			T* operator ->() const noexcept;

			T* get() const noexcept;

		private:
			T* m_data{};

		};

		template <typename T> bool operator ==(const observer_ptr<T>& lhs, std::nullptr_t rhs) noexcept;
		template <typename T> bool operator ==(std::nullptr_t lhs, const observer_ptr<T>& rhs) noexcept;
		template <typename T> bool operator !=(const observer_ptr<T>& lhs, std::nullptr_t rhs) noexcept;
		template <typename T> bool operator !=(std::nullptr_t lhs, const observer_ptr<T>& rhs) noexcept;
	}
}

#include "observer_ptr.tpp"