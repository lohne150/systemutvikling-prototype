#include "GLFWCallbackFunctions.h"

void windowCloseCallback(GLFWwindow* window)
{
	glfwSetWindowShouldClose(window, true);
}