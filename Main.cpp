#include "GameObject.h"
#include "IncMap.h"
#include "Engine.h"
#include <functional>
#include "Image.h"
#include "Texture.h"
#include "Mesh.h"
#include "InputHandler.h"
#include <random>

int main()
{

	tl::core::Engine engine;
	engine.initializeEngine();

	tl::graphics::Image image;
	image.create(glm::uvec2(100, 200), tl::graphics::Color(128, 128, 255, 255));
	tl::graphics::Texture texture("testTexture", image);

	engine.loadScene("res/scenes/forSystem.xml");


	while (engine.isActive())
	{
		engine.gameLoop();
	}

	return 0;
}