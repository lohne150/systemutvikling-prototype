#pragma once
#include <cstdint>

#define LOGGING

#ifdef LOGGING
#define CONSOLEDEBUGGING
#endif

namespace tl
{
	// 8 bits integer types
	using Uint8 = uint_fast8_t;
	using Int8 = int_fast8_t;
	
	// 16 bits integer types
	using Uint16 = uint_fast16_t;
	using Int16 = int_fast16_t;
	
	// 32 bits integer types
	using Uint32 = uint_fast32_t;
	using Int32 = int_fast32_t;
	
	// 64 bits integer types
	using Uint64 = uint_fast64_t;
	using Int64 = int_fast64_t;

	constexpr char* SETTINGS_FILE_PATH = "res/Settings.xml";
	constexpr Uint32 MAX_PARTICLES = 100000;
}