#include "MapKey.h"
#include "IncMap.h"

namespace tl
{
	namespace system
	{
		MapKey::MapKey()
		{
		}

		MapKey::MapKey(const std::string & name)
			: m_name(name)
		{
		}

		MapKey::~MapKey()
		{
		}

		void MapKey::setIndex(size_t index) noexcept
		{
			m_index = index;
			m_hasIndex = true;
		}

		void MapKey::setName(const std::string & name) noexcept
		{
			m_name = name;
			m_hasIndex = false;
		}

		const std::string & MapKey::getName() const noexcept
		{
			return m_name;
		}

		size_t MapKey::getIndex() const
		{
			if (!m_hasIndex)
			{
				throw 0;	//TODO
			}

			return m_index;
		}


		bool operator == (const MapKey& lhs, const MapKey& rhs) noexcept
		{
			return lhs.getName() == rhs.getName();
		}

		bool operator != (const MapKey& lhs, const MapKey& rhs) noexcept
		{
			return !(lhs == rhs);
		}
	}
}