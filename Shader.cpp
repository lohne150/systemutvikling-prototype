//Trond Lohne 141414

#include <stdio.h>
#include <fstream>
#include <algorithm>
#include <glm\gtc\type_ptr.hpp>
#include "RenderingEngine.h"
#include "Shader.h"
#include "StringUtils.h"
#include "LogManager.h"


typedef std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> structMemberHashMap;

namespace tl
{
	namespace graphics
	{
		////////////////////////////////////////////////////////////
		// Static member data
		////////////////////////////////////////////////////////////
		std::unordered_map<std::string, Shader::ShaderResource> Shader::s_shaderResources;
		
		////////////////////////////////////////////////////////////
		Shader::Shader() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		Shader::Shader(const std::string& shaderName) noexcept
		{
			//If this shaded hasn't been created, create it. Otherwise returned the already existing one
			if (s_shaderResources.count(shaderName) == 0)
			{
				m_resource.program = glCreateProgram();

				if (m_resource.program != NULL)
				{
					addVertexShaderFromFile(shaderName + ".vs");
					addFragmentShaderFromFile(shaderName + ".fs");

					compileShader();

					addAllUniforms(loadShaderFromFile(shaderName + ".vs"));
					addAllUniforms(loadShaderFromFile(shaderName + ".fs"));

					s_shaderResources.emplace(shaderName, m_resource);
				}
				else
				{
					LogManager::log("Shader creation failed!\n");
				}
			}
			else
			{
				m_resource = s_shaderResources[shaderName];
			}
		}

		////////////////////////////////////////////////////////////
		Shader::~Shader() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		void Shader::addVertexShaderFromFile(const std::string & filePath) noexcept
		{
			addVertexShader(loadShaderFromFile(filePath));
		}

		////////////////////////////////////////////////////////////
		void Shader::addFragmentShaderFromFile(const std::string & filePath) noexcept
		{
			addFragmentShader(loadShaderFromFile(filePath));
		}

		////////////////////////////////////////////////////////////
		void Shader::addGeometryShaderFromFile(const std::string & filePath) noexcept
		{
			addGeometryShader(loadShaderFromFile(filePath));
		}

		////////////////////////////////////////////////////////////
		void Shader::addVertexShader(const std::string& vertexShader) noexcept
		{
			addProgram(vertexShader, GL_VERTEX_SHADER);
		}

		////////////////////////////////////////////////////////////
		void Shader::addFragmentShader(const std::string & fragmentShader) noexcept
		{
			addProgram(fragmentShader, GL_FRAGMENT_SHADER);
		}

		////////////////////////////////////////////////////////////
		void Shader::addGeometryShader(const std::string & geometryShader) noexcept
		{
			addProgram(geometryShader, GL_GEOMETRY_SHADER);
		}

		////////////////////////////////////////////////////////////
		void Shader::addProgram(const std::string & shader, int type) noexcept
		{
			//Create the shader
			GLuint shaderId = glCreateShader(type);
			if (shaderId == NULL)
			{
				LogManager::log("Failed to add program to shader!\n");
			}

			int infoLogLength = 0;

			//Apply the shader code to the shader
			const GLchar* cShader = shader.c_str();
			glShaderSource(shaderId, 1, &cShader, NULL);

			//Compile the shader and check for errors
			glCompileShader(shaderId);
			glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
			if (infoLogLength > 0)
			{
				std::string shaderErrorMessage;
				shaderErrorMessage.resize(infoLogLength + 1);

				glGetShaderInfoLog(shaderId, infoLogLength, NULL, &shaderErrorMessage[0]);
				printf("%s\n", &shaderErrorMessage[0]);
			}

			//Attach the shader to the program
			glAttachShader(m_resource.program, shaderId);

			glDeleteShader(shaderId);
		}

		////////////////////////////////////////////////////////////
		void Shader::compileShader() noexcept
		{
			int infoLogLength = 0;

			//Link all the shaders in the shader program, and check for errors
			glLinkProgram(m_resource.program);
			glGetShaderiv(m_resource.program, GL_INFO_LOG_LENGTH, &infoLogLength);
			if (infoLogLength > 0)
			{
				std::string shaderErrorMessage;
				shaderErrorMessage.resize(infoLogLength + 1);

				glGetProgramInfoLog(m_resource.program, infoLogLength, NULL, &shaderErrorMessage[0]);
				printf("%s\n", &shaderErrorMessage[0]);
			}

			//Validate the program and check for any errors
			glValidateProgram(m_resource.program);
			glGetShaderiv(m_resource.program, GL_INFO_LOG_LENGTH, &infoLogLength);
			if (infoLogLength > 0)
			{
				std::string shaderErrorMessage;
				shaderErrorMessage.resize(infoLogLength + 1);

				glGetProgramInfoLog(m_resource.program, infoLogLength, NULL, &shaderErrorMessage[0]);
				printf("%s\n", &shaderErrorMessage[0]);
			}
		}

		void Shader::bind() const noexcept
		{
			glUseProgram(m_resource.program);
		}

		////////////////////////////////////////////////////////////
		//Finds all the uniforms in the shader code that are structs, 
		//and makes a vector of the structs which contains all the member variables with both type and name.
		//All the structs-data are afterwards returned in the form of a hashmap
		structMemberHashMap  Shader::findUniformStructs(const std::string& shaderProgram) noexcept
		{
			structMemberHashMap result;
			size_t structPos = shaderProgram.find("struct");

			while (structPos != std::string::npos)
			{
				std::string structName = findStructName(shaderProgram, structPos);

				//Checks if the struct has already been added to the hashmap
				if (result.count(structName) == 0)
				{
					std::vector<std::pair<std::string, std::string>> structMembers;

					//Gets the string of text that is between the closing and opening curlybrackets of the current struct
					std::string structContent = findStructContent(shaderProgram, structPos);
					removeCharFromString(structContent, '\n');

					size_t endName = structContent.find_last_of(";");
					while (endName != std::string::npos)
					{
						//Find the first and last position of the letters in the name of the member variable
						endName = structContent.find_last_not_of(" ", endName - 1);
						size_t beginName = structContent.find_last_of(" ", endName - 1) + 1;

						//Find the first and last position of the letters in the type of the member variable
						size_t endType = structContent.find_last_not_of(" ", beginName - 1);
						size_t beginType = structContent.find_last_of(" ", endType - 1) + 1;

						std::string type = structContent.substr(beginType, endType - beginType + 1);
						std::string name = structContent.substr(beginName, endName - beginName + 1);

						structMembers.push_back(std::pair<std::string, std::string>(type, name));

						//Find the next member variable
						endName = structContent.find_last_of(";", beginType - 1);
					}

					result.emplace(structName, structMembers);
					structPos = shaderProgram.find("struct", structPos + 1);
				}
			}

			return result;
		}

		////////////////////////////////////////////////////////////
		//Automaticly adds all the uniforms of a shader program
		//Also takes into consideration if a uniform is a struct, and resolves it
		void Shader::addAllUniforms(std::string & shaderProgram) noexcept
		{
			auto structs = findUniformStructs(shaderProgram);

			//Removes certain characters from the shader code, so it is easier to work with
			//This results in having a vector of strings where each element of the vector contains
			//one line each, and each line does not have semicolons, tabs or newlines
			removeCharFromString(shaderProgram, ';');
			removeCharFromString(shaderProgram, '\t');
			auto lines = splitString(shaderProgram, '\n');
			lines.erase(std::remove(lines.begin(), lines.end(), ""), lines.end());

			//Used to know which line we are currently on in the for loop
			int i = 0;

			for (auto& line : lines)
			{
				//If the line contains the work uniform
				if (line.find("uniform") != std::string::npos)
				{
					//Try to split the line into parts that each contains:
					//1. uniform
					//2. type
					//3. name
					auto tokens = splitString(line, ' ');

					//If the line has the correct syntax, add the uniform
					if (tokens.size() == 3)
					{
						addUniform(tokens[2], tokens[1], structs);
					}
					else
					{
						printf("Error: Incorrect syntax at line %i\n%s\n", i, line.c_str());
					}

					m_resource.uniformTypeAndNames.push_back((std::pair<std::string, std::string>(tokens[1], tokens[2])));
				}
				i++;
			}

		}

		////////////////////////////////////////////////////////////
		void Shader::addUniform(const std::string & uniformName,
			const std::string & uniformType,
			std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& structs) noexcept
		{
			//If the member variable is a struct
			if (structs.count(uniformType) == 1)
			{
				for (auto& structMember : structs[uniformType])
				{
					//Recursivly adds the uniform with its member variable. This also handles a struct
					//containing a member variable of type struct
					addUniform(uniformName + "." + structMember.second, structMember.first, structs);
				}
			}
			else
			{
				if (uniformName.find_first_of('[') != std::string::npos)
				{
					Uint32 bracketPos = uniformName.find_first_of('[');

					std::string newUniformName = uniformName.substr(0, bracketPos);
					Uint32 instances = std::stoi(uniformName.substr(bracketPos + 1, uniformName.find_first_of(']') - bracketPos));

					for (Uint32 i = 0; i < instances; i++)
					{
						std::string finalUniformName = newUniformName + '[' + std::to_string(i) + ']';

						if (m_resource.uniforms.count(finalUniformName) > 0)
						{
							return;
						}

						GLuint uniformLocation = glGetUniformLocation(m_resource.program, finalUniformName.c_str());
						if (uniformLocation == -1)
						{
							printf("Error: Could not find uniform: %s\n", finalUniformName.c_str());
							return;
						}

						m_resource.uniforms.emplace(finalUniformName, uniformLocation);
					}
				}
				else
				{
					if (m_resource.uniforms.count(uniformName) > 0)
					{
						return;
					}

					GLuint uniformLocation = glGetUniformLocation(m_resource.program, uniformName.c_str());
					if (uniformLocation == -1)
					{
						printf("Error: Could not find uniform: %s\n", uniformName.c_str());
						return;
					}

					m_resource.uniforms.emplace(uniformName, uniformLocation);
				}
			}
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, GLuint value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform1ui(m_resource.uniforms[uniformName], value);
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, GLint value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform1i(m_resource.uniforms[uniformName], value);
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, GLfloat value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform1f(m_resource.uniforms[uniformName], value);
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, const glm::vec2 & value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform2fv(m_resource.uniforms[uniformName], 1, glm::value_ptr(value));
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, const glm::vec3& value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform3fv(m_resource.uniforms[uniformName], 1, glm::value_ptr(value));
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, const glm::vec4& value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniform4fv(m_resource.uniforms[uniformName], 1, glm::value_ptr(value));
		}

		////////////////////////////////////////////////////////////
		void Shader::setUniform(const std::string & uniformName, const glm::mat4 & value) noexcept
		{
			if (m_resource.uniforms.count(uniformName) == 0)
			{
				printf("Error: No uniform with name: %s\n", uniformName.c_str());
				return;
			}

			glUniformMatrix4fv(m_resource.uniforms[uniformName], 1, GL_FALSE, glm::value_ptr(value));
		}

		////////////////////////////////////////////////////////////
		std::string Shader::loadShaderFromFile(const std::string & filePath) noexcept
		{
			std::string shaderCode;
			std::ifstream shaderStream("res/shaders/" + filePath, std::ios::in);

			if (shaderStream)
			{
				std::string line;

				while (getline(shaderStream, line))
				{
					//If the line has the word #include, try to load the file
					if (line.find("#include") != std::string::npos)
					{
						auto tokens = splitString(line, '\"');

						shaderCode += loadShaderFromFile(tokens[1]);
					}
					else
					{
						shaderCode += '\n' + line;
					}
				}

				shaderStream.close();
			}
			else
			{
				printf("Error: Could not open %s\n", filePath.c_str());
			}

			return shaderCode;
		}

		std::string Shader::findStructName(const std::string & shaderProgram, size_t structStart) noexcept
		{
			size_t beginStructName = shaderProgram.find_first_of(" ", structStart + 1);
			beginStructName = shaderProgram.find_first_not_of(" ", beginStructName + 1);
			size_t endStructName = shaderProgram.find_first_of(" \n{", beginStructName + 1);

			return shaderProgram.substr(beginStructName, endStructName - beginStructName);
		}

		std::string Shader::findStructContent(const std::string & shaderProgram, size_t structStart) noexcept
		{
			size_t beginStruct = shaderProgram.find("{", structStart + 1);
			size_t endStruct = shaderProgram.find("}", beginStruct + 1);

			return shaderProgram.substr(beginStruct + 1, endStruct - beginStruct - 1);
		}
	}
}

