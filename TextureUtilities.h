#pragma once
#include <string>
#include "tinyxml2.h"

namespace tl
{
	bool checkTexturesType(const std::string& imageType) noexcept;
	void loadSupportedTexturesTypes() noexcept;
}