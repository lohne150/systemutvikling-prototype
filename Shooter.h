#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class Shooter : public core::Component
		{
		public:
			Shooter(Uint32 ownerId) noexcept;
			Shooter(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~Shooter() noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
		};
	}
}
