#pragma once
#include <atomic>
#include <glm\glm.hpp>
#include "Texture.h"

namespace tl
{ 
	namespace graphics
	{
		struct Particle
		{
			glm::vec3 position;
			glm::vec3 velocity;
			float timeLeft{};
			float totalTime{};
			float visibility;
			float distance;
			Texture texture;
		};

		bool operator<(const Particle& lhs, const Particle& rhs) noexcept;
	}
}