#include <vector>
#include "stb_image.h"
#include "Image.h"
#include "OpenGL.h"
#include "LogManager.h"
#include "TextureUtilities.h"

namespace tl
{
	namespace graphics
	{
		////////////////////////////////////////////////////////////
		Image::Image() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		Image::Image(const std::string& imageFilePath) noexcept
		{
			loadImageFromFile(imageFilePath);
		}

		////////////////////////////////////////////////////////////
		Image::~Image() noexcept
		{
		}

		////////////////////////////////////////////////////////////
		void Image::create(const glm::uvec2 & size, const Color& color) noexcept
		{
			LogManager::log("Creating image of size (%u, %u) and color (%u, %u, %u, %u)\n", size.x, size.y, color.r, color.g, color.b, color.a);

			if (size.x > 0 && size.y > 0)
			{
				m_size = size;
				m_pixels.resize(size.x * size.y * 4);

				for (auto iter = m_pixels.begin(); iter != m_pixels.end();)
				{
					*iter++ = color.r;
					*iter++ = color.g;
					*iter++ = color.b;
					*iter++ = color.a;
				}
			}
			else
			{
				m_size.x = 0;
				m_size.y = 0;
				m_pixels.clear();
			}
		}

		////////////////////////////////////////////////////////////
		void Image::create(const glm::uvec2& size, const Uint8* pixels) noexcept
		{
			LogManager::log("Creating image of size (%u, %u)\n", size.x, size.y);

			if (size.x > 0 && size.y > 0 && pixels != nullptr)
			{
				m_size = size;

				Uint32 size = m_size.x * m_size.y * 4;
				m_pixels.resize(size);
				std::memcpy(&m_pixels.front(), pixels, size);
			}
			else
			{
				m_size.x = 0;
				m_size.y = 0;
				m_pixels.clear();
			}
		}

		////////////////////////////////////////////////////////////
		bool Image::loadImageFromFile(const std::string& imageFilePath) noexcept
		{
			LogManager::log("Loading image from path \"%s\"...", imageFilePath.c_str());

			std::string fileType = imageFilePath.substr(imageFilePath.find_first_of('.'));

			if (!checkTexturesType(fileType))
			{
				LogManager::log("FILE FORMAT NOT SUPPORTED FOR IMAGE DATA \"%s\"\n", fileType.c_str());
				return false;
			}

			Int32 channels = 0;
			Int32 width = 0;
			Int32 height = 0;
			Uint8* pixels = stbi_load(imageFilePath.c_str(), &width, &height, &channels, STBI_rgb_alpha);

			if (pixels && width && height)
			{
				m_size.x = width;
				m_size.y = height;
				Uint32 size = width * height * 4;
				m_pixels.resize(size);
				std::memcpy(&m_pixels.front(), pixels, size);

				LogManager::log("loaded!\n");
				return true;
			}
			else
			{
				m_size.x = 0;
				m_size.y = 0;
				m_pixels.clear();

				LogManager::log("COULD NOT LOAD IMAGE \"%s\"\n", imageFilePath.c_str());
				return false;
			}
		}

		////////////////////////////////////////////////////////////
		void Image::setPixel(Uint32 x, Uint32 y, const Color& color) noexcept
		{
			Uint8* pixel = &m_pixels[(x + y * m_size.x) * 4];
			*pixel++ = color.r;
			*pixel++ = color.g;
			*pixel++ = color.b;
			*pixel++ = color.r;
		}

		////////////////////////////////////////////////////////////
		const glm::uvec2 & Image::getSize() const noexcept
		{
			return m_size;
		}

		////////////////////////////////////////////////////////////
		Color Image::getPixel(Uint32 x, Uint32 y) const noexcept
		{
			const Uint8* pixel = &m_pixels[(x + y * m_size.x) * 4];
			return Color(pixel[0], pixel[1], pixel[2], pixel[3]);
		}

		////////////////////////////////////////////////////////////
		const Uint8 * Image::getPixelsPtr() const noexcept
		{
			if (!m_pixels.empty())
			{
				return &m_pixels.front();
			}
			else
			{
				LogManager::log("TRYING TO ACCES THE PIXELS OF AN EMPTY IMAGE!\n");
				return nullptr;
			}
		}

	}
}