#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class FreeLook : public core::Component
		{
		public:
			FreeLook(Uint32 ownerId) noexcept;
			FreeLook(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			FreeLook(Uint32 ownerId, float speed) noexcept;
			FreeLook(Uint32 ownerId, float speed, Key upKey, Key downKey, Key leftKey, Key rightKey) noexcept;
			virtual ~FreeLook();

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			float m_speed{ 1.0f };
			Key m_upKey{ Key::Up };
			Key m_downKey{ Key::Down };
			Key m_leftKey{ Key::Left };
			Key m_rightKey{ Key::Right };

			static const float c_upLimit;
			static const float c_downLimit;
		};
	}
}