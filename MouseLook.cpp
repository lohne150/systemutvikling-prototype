#include "MouseLook.h"

namespace tl
{
	namespace components
	{
		const std::string MouseLook::type = "MouseLook";

		MouseLook::MouseLook(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		MouseLook::MouseLook(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		MouseLook::MouseLook(Uint32 ownerId, float speed) noexcept
			: Component(type, ownerId)
		{
		}

		MouseLook::~MouseLook()
		{
		}


		const std::string& MouseLook::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> MouseLook::clone() const noexcept
		{
			return std::make_unique<MouseLook>(*this);
		}

		void MouseLook::start(graphics::Window& window) noexcept
		{
			window.setInputMode(GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}

		void MouseLook::update(graphics::Window& window, float deltaTime) noexcept
		{
			if (window.getInputMode(GLFW_CURSOR) == GLFW_CURSOR_DISABLED)
			{
				core::Transform& transform = s_engine->getGameObject(m_ownerId).getTransform();
				glm::vec2 mouseDelta = InputHandler::getMouseDelta();
				transform.rotate(-m_speed * mouseDelta.x * deltaTime, glm::vec3(0, 1, 0));
				transform.rotate(m_speed * mouseDelta.y * deltaTime, transform.getRightDirection());
			}

			if (InputHandler::isKeyDown(Key::Escape))
			{
				window.setInputMode(GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}
			if (InputHandler::isButtonDown(Button::LMouse))
			{
				window.setInputMode(GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			}
		}

		void MouseLook::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}