#include <vector>
#include <boost\filesystem.hpp>
#include "MeshUtilities.h"
#include "Config.h"
#include "XMLUtilities.h"

namespace tl
{
	////////////////////////////////////////////////////////////
	// Static member data
	////////////////////////////////////////////////////////////
	std::vector<std::string> s_supportedMeshTypes;

	////////////////////////////////////////////////////////////
	bool checkMeshType(const std::string& meshType) noexcept
	{
		for (auto&& supportedType : s_supportedMeshTypes)
		{
			if (meshType == supportedType)
			{
				return true;
			}
		}

		return false;
	}

	////////////////////////////////////////////////////////////
	void loadSupportedMeshTypes() noexcept
	{
		tinyxml2::XMLDocument document;
		document.LoadFile(SETTINGS_FILE_PATH);
		tinyxml2::XMLElement* typeElement = getElement(document, "Settings/Meshes/SupportedTypes/Type");

		while (typeElement)
		{
			s_supportedMeshTypes.push_back(typeElement->GetText());
			typeElement = typeElement->NextSiblingElement("Type");
		}
	}
}