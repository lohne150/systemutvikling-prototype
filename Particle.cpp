#include "Particle.h"

bool tl::graphics::operator<(const Particle & lhs, const Particle & rhs) noexcept
{
	return lhs.distance > rhs.distance;
}
