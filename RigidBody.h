#pragma once
#include "Component.h"

namespace tl
{
	namespace components
	{
		class RigidBody : public core::Component
		{
		public:
			RigidBody(Uint32 ownerId) noexcept;
			RigidBody(tinyxml2::XMLElement* xmlElement, Uint32 ownerId) noexcept;
			virtual ~RigidBody() noexcept;

			void setVelocity(const glm::vec3 velocity) noexcept;
			void setAngularVelocity(const glm::vec3 angularVelocity) noexcept;

			void loadFromXML(tinyxml2::XMLElement* xmlElement) noexcept;

			virtual const std::string& getComponentType() const noexcept override;
			virtual std::unique_ptr<core::IComponent> clone() const noexcept override;
			virtual void start(graphics::Window& window) noexcept override;
			virtual void update(graphics::Window& window, float deltaTime) noexcept override;
			virtual void render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept override;

			static const std::string type;

		private:
			glm::vec3 m_velocity{};
			glm::vec3 m_angularVelocity{ 0, 0, 0 };
			float m_gravitationalForce{ 10.0f };

		};
	}
}
