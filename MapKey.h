#pragma once
#include <string>

namespace tl
{
	namespace system
	{
		template <typename T>
		class IncMap;

		class MapKey
		{
			template <typename T>
			friend class IncMap;

		public:
			MapKey();
			MapKey(const std::string& name);
			~MapKey();

			void setName(const std::string& name) noexcept;

			const std::string& getName() const noexcept;
			size_t getIndex() const;

		private:
			std::string m_name{};
			size_t m_index{};
			bool m_hasIndex{};

		private:
			void setIndex(size_t index) noexcept;

		};

		bool operator == (const MapKey& lhs, const MapKey& rhs) noexcept;
		bool operator != (const MapKey& lhs, const MapKey& rhs) noexcept;
	}
}
