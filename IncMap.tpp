#pragma once
#include "IncMap.h"

namespace tl
{
	namespace system
	{
		template<typename T>
		IncMap<T>::IncMap() noexcept
		{
			m_container.reserve(1000);
			m_metaData.reserve(1000);
		}

		template<typename T>
		IncMap<T>::~IncMap() noexcept
		{
		}

		template<typename T>
		bool IncMap<T>::validateName(const std::string & name, Uint32& index) noexcept
		{
			if (m_metaData.empty())
			{
				return false;
			}

			auto iter = m_metaData.find(name);

			if (iter != std::end(m_metaData))
			{
				index = iter->second;
				return true;
			}

			return false;
		}

		template<typename T>
		inline std::vector<T>& IncMap<T>::data() noexcept
		{
			return m_container;
		}

		template<typename T>
		inline size_t IncMap<T>::size() const noexcept
		{
			return m_container.size();
		}

		template<typename T>
		void IncMap<T>::push_back(const T& entry, const std::string & name)
		{
			if (m_metaData.find(name) != std::end(m_metaData))
			{
				throw 0; //TODO
			}

			m_metaData.emplace(name, m_container.size());
			m_container.push_back(entry);
		}

		template<typename T>
		T & IncMap<T>::back() noexcept
		{
			return m_container.back();
		}

		template<typename T>
		T& IncMap<T>::operator[](MapKey& key)
		{
			if (!key.m_hasIndex)
			{
				size_t index = 0;

				if (!validateName(key.getName(), index))
				{
					throw 0;
				}
				else
				{
					key.setIndex(index);
				}
			}

			return m_container[key.getIndex()];
		}
		
		template<typename T>
		system::MapKey IncMap<T>::createKey(const std::string & name) noexcept
		{
			size_t index = 0;
			system::MapKey result(name);

			if (validateName(name, index))
			{
				result.setIndex(index);
			}

			return result;
		}

		template<typename T>
		bool IncMap<T>::entryExists(system::MapKey& key) noexcept
		{
			Uint32 index = 0;

			if (validateName(key.getName(), index))
			{
				key.setIndex(index);
				return true;
			}

			return false;
		}
	}
}

