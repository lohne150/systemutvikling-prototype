#include "RigidBody.h"

namespace tl
{
	namespace components
	{
		const std::string RigidBody::type = "RigidBody";

		RigidBody::RigidBody(Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
		}

		RigidBody::RigidBody(tinyxml2::XMLElement * xmlElement, Uint32 ownerId) noexcept
			: Component(type, ownerId)
		{
			loadFromXML(xmlElement);
		}

		RigidBody::~RigidBody() noexcept
		{
		}

		void RigidBody::setVelocity(const glm::vec3 velocity) noexcept
		{
			m_velocity = velocity;
		}

		void RigidBody::setAngularVelocity(const glm::vec3 angularVelocity) noexcept
		{
			m_angularVelocity = angularVelocity;
		}

		void RigidBody::loadFromXML(tinyxml2::XMLElement * xmlElement) noexcept
		{
			if (!xmlElement)
			{
				return;
			}

			tinyxml2::XMLElement* gravitationalForceElement = xmlElement->FirstChildElement("gravitationalForce");
			if (gravitationalForceElement)
			{ 
				gravitationalForceElement->QueryFloatText(&m_gravitationalForce);
			}
		}

		const std::string& RigidBody::getComponentType() const noexcept
		{
			return type;
		}

		std::unique_ptr<core::IComponent> RigidBody::clone() const noexcept
		{
			return std::make_unique<RigidBody>(*this);
		}

		void RigidBody::start(graphics::Window& window) noexcept
		{
		}

		void RigidBody::update(graphics::Window& window, float deltaTime) noexcept
		{
			core::Transform& transform = s_engine->getGameObject(m_ownerId).getTransform();
			m_velocity.y -= m_gravitationalForce * deltaTime;

			transform.translate(m_velocity * deltaTime);
			transform.rotate(m_angularVelocity.x * deltaTime, glm::vec3(1, 0, 0));
			transform.rotate(m_angularVelocity.y * deltaTime, glm::vec3(0, 1, 0));
			transform.rotate(m_angularVelocity.z * deltaTime, glm::vec3(0, 0, 1));
		}

		void RigidBody::render(graphics::Window& window, graphics::Shader& shader, core::Transform& transform) noexcept
		{
		}
	}
}