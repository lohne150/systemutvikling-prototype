#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>
#include "MapKey.h"
#include "Config.h"

namespace tl
{
	namespace system
	{
		template <typename T>
		class IncMap
		{
		public:
			IncMap() noexcept;
			~IncMap() noexcept;

			inline std::vector<T>& data() noexcept;
			inline size_t size() const noexcept;

			void push_back(const T& entry, const std::string& name);
			T& back() noexcept;
			T& operator[](MapKey& key);
			system::MapKey createKey(const std::string& name) noexcept;
			bool entryExists(system::MapKey& key) noexcept;

		private:
			bool validateName(const std::string& name, Uint32& index) noexcept;
		
		private:
			struct EntryMetaData
			{
				std::string name;
				size_t index;
			};

			std::vector<T> m_container;
			std::unordered_map<std::string, size_t> m_metaData;

		};
	}
}

#include "IncMap.tpp"