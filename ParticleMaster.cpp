#include <random>
#include "ParticleMaster.h"
#include "Camera.h"
#include "StringUtils.h"
#include "LogManager.h"
#include "tinyxml2.h"
#include "tiny_obj_loader.h"

namespace tl
{
	namespace graphics
	{

		ParticleMaster::ParticleMaster() noexcept
		{
		}


		ParticleMaster::~ParticleMaster() noexcept
		{
		}

		void ParticleMaster::initialize() noexcept
		{
			glGenVertexArrays(1, &m_meshData.vao);
			glBindVertexArray(m_meshData.vao);
			glGenBuffers(MeshInstancingData::NUM_BUFFERS, m_meshData.vbo);

			std::vector<tinyobj::shape_t> shapes;
			std::vector<tinyobj::material_t> materials;
			std::string error;
			tinyobj::LoadObj(shapes, materials, error, "res/meshes/particle.obj");
			auto mesh = shapes.front().mesh;
			m_meshData.drawCount = mesh.indices.size();

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VERTEX_VB]);
			glBufferData(GL_ARRAY_BUFFER, mesh.positions.size() * sizeof(mesh.positions.front()), mesh.positions.data(), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::TEXTURECOORDINATE_VB]);
			glBufferData(GL_ARRAY_BUFFER, mesh.texcoords.size() * sizeof(mesh.texcoords.front()), mesh.texcoords.data(), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::INDEX_VB]);
			glBufferData(GL_ARRAY_BUFFER, mesh.indices.size() * sizeof(mesh.indices.front()), mesh.indices.data(), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::POSITION_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_positions.front()), nullptr, GL_STREAM_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VISIBILITYV_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_visibility.front()), nullptr, GL_STREAM_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VISIBILITYV_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_textures.front()), nullptr, GL_STREAM_DRAW);
		}

		void ParticleMaster::update(float deltaTime, Window& window) noexcept
		{
			glm::vec3 cameraPos = window.getCamera()->getTransform().getPosition();

			for (Uint32 i = 0; i < m_numberOfActiveParticles; ++i)
			{
				m_timeLefts[i] -= deltaTime;
				m_positions[i] += m_velocities[i] * deltaTime;
				m_distances[i] = glm::length(m_positions[i] - cameraPos);
				m_visibility[i] = m_timeLefts[i] / m_totalTimes[i];

				if (m_timeLefts[i] <= 0)
				{
					removeParticle(i);
				}
			}

			sortParticlesByDistance();
		}

		void ParticleMaster::render(Window& window) noexcept
		{
			std::unordered_map<Uint32, Int32> textureIds;
			Uint32 prevTextureId = 32;
			textureIds.reserve(32);
			glm::mat4 projectionMatrix = window.getCamera()->getProjection();
			glm::mat4 viewMatrix = window.getCamera()->getView();
			glm::mat4 modelMatrix(1.0f);

			modelMatrix[0][0] = viewMatrix[0][0];
			modelMatrix[1][0] = viewMatrix[0][1];
			modelMatrix[2][0] = viewMatrix[0][2];
			modelMatrix[0][1] = viewMatrix[1][0];
			modelMatrix[1][1] = viewMatrix[1][1];
			modelMatrix[2][1] = viewMatrix[1][2];
			modelMatrix[0][2] = viewMatrix[2][0];
			modelMatrix[1][2] = viewMatrix[2][1];
			modelMatrix[2][2] = viewMatrix[2][2];

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			m_shader.bind();
			m_texture.bind();
			m_shader.setUniform("modelMatrix", projectionMatrix * viewMatrix);
			drawInstance();

			glDisable(GL_BLEND);
		}

		void ParticleMaster::addParticle(const Particle& particle) noexcept
		{
			if (m_numberOfActiveParticles >= MAX_PARTICLES)
			{
				return;
			}

			m_positions[m_numberOfActiveParticles] = particle.position;
			m_velocities[m_numberOfActiveParticles] = particle.velocity;
			m_timeLefts[m_numberOfActiveParticles] = particle.timeLeft;
			m_totalTimes[m_numberOfActiveParticles] = particle.totalTime;
			m_textures[m_numberOfActiveParticles] = particle.texture;
			m_visibility[m_numberOfActiveParticles] = 1;

			++m_numberOfActiveParticles;
		}
		void ParticleMaster::removeParticle(Uint32 i) noexcept
		{
			if (i >= m_numberOfActiveParticles)
			{
				return;
			}

			--m_numberOfActiveParticles;

			m_positions[i] = m_positions[m_numberOfActiveParticles];
			m_velocities[i] = m_velocities[m_numberOfActiveParticles];
			m_timeLefts[i] = m_timeLefts[m_numberOfActiveParticles];
			m_totalTimes[i] = m_totalTimes[m_numberOfActiveParticles];
			m_distances[i] = m_distances[m_numberOfActiveParticles];
			m_textures[i] = m_textures[m_numberOfActiveParticles];
			m_visibility[i] = m_visibility[m_numberOfActiveParticles];
		}

		void ParticleMaster::sortParticlesByDistance() noexcept
		{
			for (Uint32 i = 0; i < m_numberOfActiveParticles; ++i)
			{
				m_sortingParticles[i].position = m_positions[i];
				m_sortingParticles[i].velocity = m_velocities[i];
				m_sortingParticles[i].timeLeft = m_timeLefts[i];
				m_sortingParticles[i].totalTime = m_totalTimes[i];
				m_sortingParticles[i].distance = m_distances[i];
				m_sortingParticles[i].texture = m_textures[i];
				m_sortingParticles[i].visibility = m_visibility[i];
			}

			std::sort(m_sortingParticles.begin(), m_sortingParticles.begin() + m_numberOfActiveParticles);

			for (Uint32 i = 0; i < m_numberOfActiveParticles; ++i)
			{
				m_positions[i] = m_sortingParticles[i].position;
				m_velocities[i] = m_sortingParticles[i].velocity;
				m_timeLefts[i] = m_sortingParticles[i].timeLeft;
				m_totalTimes[i] = m_sortingParticles[i].totalTime;
				m_distances[i] = m_sortingParticles[i].distance;
				m_textures[i] = m_sortingParticles[i].texture;
				m_visibility[i] = m_sortingParticles[i].visibility;
			}
		}
		void ParticleMaster::drawInstance() noexcept
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::POSITION_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_positions.front()), nullptr, GL_STREAM_DRAW);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_numberOfActiveParticles * sizeof(m_positions.front()), m_positions.data());

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VISIBILITYV_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_visibility.front()), nullptr, GL_STREAM_DRAW);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_numberOfActiveParticles * sizeof(m_visibility.front()), m_visibility.data());

			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::TEXTURE_VB]);
			glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * sizeof(m_textures.front()), nullptr, GL_STREAM_DRAW);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_numberOfActiveParticles * sizeof(m_textures.front()), m_textures.data());

			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VERTEX_VB]);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::TEXTURECOORDINATE_VB]);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(2);
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::POSITION_VB]);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(3);
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::VISIBILITYV_VB]);
			glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(4);
			glBindBuffer(GL_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::TEXTURE_VB]);
			glVertexAttribPointer(4, 1, GL_UNSIGNED_INT, GL_FALSE, 0, 0);

			glVertexAttribDivisor(0, 0);
			glVertexAttribDivisor(1, 0);
			glVertexAttribDivisor(2, 1);
			glVertexAttribDivisor(3, 1);
			glVertexAttribDivisor(4, 1);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_meshData.vbo[MeshInstancingData::INDEX_VB]);
			glDrawElementsInstanced(GL_TRIANGLES, m_meshData.drawCount, GL_UNSIGNED_INT, NULL, m_numberOfActiveParticles);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glDisableVertexAttribArray(2);
			glDisableVertexAttribArray(3);
			glDisableVertexAttribArray(4);
		}
	}
}