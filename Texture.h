#pragma once
#include <unordered_map>
#include <glm\glm.hpp>
#include "OpenGL.h"
#include "Config.h"
#include "Image.h"


namespace tl
{
	namespace graphics
	{
		class Texture
		{
		public:
			struct TextureData
			{
				GLuint textureId{};
			};

			Texture() noexcept;
			Texture(const std::string& textureName) noexcept;
			Texture(const std::string& textureName, const Image& image) noexcept;
			~Texture() noexcept;

			TextureData getData() const noexcept;

			void bind(Uint32 textureSlot = GL_TEXTURE0) noexcept;
			void loadTextureFromFile(const std::string& textureName, const std::string& textureFilePath) noexcept;

		private:
			bool createTexture(const std::string& textureName, const Image& image) noexcept;

		private:			
			TextureData m_data{};

			static std::unordered_map<std::string, TextureData> s_textures;
		};

		constexpr size_t size = sizeof(Texture);
	}
}

namespace std
{
	template <>
	struct hash<tl::graphics::Texture>
	{
		std::size_t operator()(const tl::graphics::Texture& texture)
		{
			return std::hash<std::size_t>()(texture.getData().textureId);
		}
	};
}